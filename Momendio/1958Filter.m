//
//  1958Filter.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 11/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "1958Filter.h"
#import "GPUImagePicture.h"
#import "GPUImageLookupFilter.h"

@implementation _958Filter

- (id)init
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
    UIImage *image = [UIImage imageNamed:@"lookup_1958.png"];
#else
    NSImage *image = [NSImage imageNamed:@"lookup_1958"];
#endif
    
    NSAssert(image, @"To use 1958Filter you need to add lookup_1958 in your application bundle.");
    
    lookupImageSource = [[GPUImagePicture alloc] initWithImage:image];
    GPUImageLookupFilter *lookupFilter = [[GPUImageLookupFilter alloc] init];
    
    [lookupImageSource addTarget:lookupFilter atTextureLocation:1];
    [lookupImageSource processImage];
    
    self.initialFilters = [NSArray arrayWithObjects:lookupFilter, nil];
    self.terminalFilter = lookupFilter;
    
    return self;
}

@end
