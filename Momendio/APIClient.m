//
//  APIClient.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 22/10/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "APIClient.h"

@implementation APIClient

+ (id)sharedInstance {
    static APIClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[APIClient alloc] initWithBaseURL:
                            [NSURL URLWithString:@"http://momendio-api.herokuapp.com/"]];
    });
    
    return __sharedInstance;
}

- (id)initWithBaseURL:(NSURL *)url {
    
    self = [super initWithBaseURL:url];
    
    if (self) {
        
        [self setParameterEncoding: AFJSONParameterEncoding];
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(appWillResignActive)
                   name:UIApplicationWillResignActiveNotification
                 object:nil];
    }
    
    return self;
}

// Allow network activity to continue when the home button is pressed
- (void)appWillResignActive {
    for (AFHTTPRequestOperation *operation in self.operationQueue.operations) {
        [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
            NSLog(@"background task cancelled");
        }];
    }
}

@end
