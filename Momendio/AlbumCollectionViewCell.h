//
//  AlbumCollectionViewCell.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 21/1/14.
//  Copyright (c) 2014 Nur Iman Izam Othman. All rights reserved.
//

#import "PSTCollectionViewCell.h"

@interface AlbumCollectionViewCell : PSTCollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *albumImageView;
@property (strong, nonatomic) IBOutlet UILabel *albumNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *albumGenreLabel;

- (void)setStyling;

@end
