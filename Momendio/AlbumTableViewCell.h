//
//  AlbumTableViewCell.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *albumImageView;
@property (strong, nonatomic) IBOutlet UILabel *albumTitle;
@property (strong, nonatomic) IBOutlet UILabel *albumGenre;

- (void)setStyling;

@end
