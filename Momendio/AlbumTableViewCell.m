//
//  AlbumTableViewCell.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "AlbumTableViewCell.h"

@implementation AlbumTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setStyling {
    [self.albumTitle setFont:[UIFont fontWithName:@"Avenir-Medium" size:17.0f]];
    [self.albumGenre setFont:[UIFont fontWithName:@"Avenir-Light" size:15.0f]];
}

@end
