//
//  AppDelegate.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 9/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)openFacebookSession;
- (void)closeFacebookSession;
- (BOOL)isFacebookLoggedIn;

- (void)showProjectOverlayIfNeeded;
- (void)showEditOverlayIfNeeded;
- (void)showFilterOverlayIfNeeded;

@end
