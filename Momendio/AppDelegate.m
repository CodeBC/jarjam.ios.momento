//
//  AppDelegate.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 9/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "AppDelegate.h"
#import "YouTubeController.h"
#import <MediaPlayer/MediaPlayer.h>

//#import <Crashlytics/Crashlytics.h>

#import <FacebookSDK/FacebookSDK.h>
#import "ProjectOverlayViewController.h"
#import "EditOverlayViewController.h"
#import "FilterOverlayViewController.h"

@interface AppDelegate ()

@property (assign, nonatomic) BOOL rotationEnabled;

@property (strong, nonatomic) ProjectOverlayViewController *projectOverlayView;
@property (strong, nonatomic) EditOverlayViewController *editOverlayView;
@property (strong, nonatomic) FilterOverlayViewController *filterOverlayView;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self globalStyling];
    
    // Setup MagicalRecord
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    // Setup DDLog
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    // Setup Crashlytics
//    [Crashlytics startWithAPIKey:@"be5731fac922d29f3b102c5b27f6f4f36e8991f0"];
    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded || [[YouTubeController sharedInstance].auth canAuthorize]) { // User is logged in to one of these.
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        UIViewController *loggedinViewController = [storyboard instantiateViewControllerWithIdentifier:@"MomendioNavigation"];
        self.window.rootViewController = loggedinViewController;
        
        // Refresh session for Facebook if exists
        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
            // Yes, so just open the session (this won't display any UX).
            [self openFacebookSession];
        }
        
        // Reauthorize YouTube if available
        if ([[YouTubeController sharedInstance].auth canAuthorize]) {
            DDLogVerbose(@"User canAuthorize");
            [[YouTubeController sharedInstance] authorizeAccessTokenWithcompleteHandler:^{
                
            }];
        }
    }
    
    // Set Tutorial If Key doesn't exists
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:kTutorialDefaultskey]) {
        [defaults setBool:YES forKey:kTutorialDefaultskey];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayerDidEnterFullScreen:) name:MPMoviePlayerWillEnterFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayerDidExitFullScreen:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    
    self.rotationEnabled = NO;
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [MagicalRecord cleanUp];
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (self.rotationEnabled) {
        return UIInterfaceOrientationMaskLandscape |UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

#pragma mark - Facebook

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            [[NSNotificationCenter defaultCenter] postNotificationName:FacebookDidLoginSuccess object:nil];
            break;
            
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            
            [FBSession.activeSession closeAndClearTokenInformation];
            [[NSNotificationCenter defaultCenter] postNotificationName:FacebookDidLoginFail object:nil];
            break;
            
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)openFacebookSession
{
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info",@"email"]
                                          allowLoginUI:YES
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
        [self sessionStateChanged:session state:state error:error];
    }];
}

- (BOOL)isFacebookLoggedIn {
    return [FBSession.activeSession isOpen];
}

- (void)closeFacebookSession {
    [FBSession.activeSession closeAndClearTokenInformation];
}

#pragma mark - Overlay Functions

- (void)showProjectOverlayIfNeeded {
    BOOL showOverlay = [[NSUserDefaults standardUserDefaults] boolForKey:kTutorialDefaultskey];
    
    if (showOverlay) {
        
        if (!self.projectOverlayView) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            self.projectOverlayView = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProjectOverlay"];
        }
        
        [self.window.rootViewController.view addSubview:self.projectOverlayView.view];
    }
}

- (void)showEditOverlayIfNeeded {
    BOOL showOverlay = [[NSUserDefaults standardUserDefaults] boolForKey:kTutorialDefaultskey];
    
    if (showOverlay) {
        
        if (!self.editOverlayView) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            self.editOverlayView = [mainStoryboard instantiateViewControllerWithIdentifier:@"EditOverlay"];
        }
        
        [self.window.rootViewController.view addSubview:self.editOverlayView.view];
        
    }
}

- (void)showFilterOverlayIfNeeded {
    
    BOOL showOverlay = [[NSUserDefaults standardUserDefaults] boolForKey:kTutorialDefaultskey];
    
    if (showOverlay) {
        
        if (!self.filterOverlayView) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            self.filterOverlayView = [mainStoryboard instantiateViewControllerWithIdentifier:@"FilterOverlay"];
        }
        
        [self.window.rootViewController.view addSubview:self.filterOverlayView.view];
        
    }
}

#pragma mark - Helper Functions

- (void)globalStyling {
    
    UIImage *navBarImage = [UIImage imageNamed:@"navbar"];
    [[UINavigationBar appearance] setBackgroundImage:navBarImage forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                     UITextAttributeFont: [UIFont fontWithName:@"Avenir-Medium" size:19.0f],
                                UITextAttributeTextColor: [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1] }];
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1]];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)videoPlayerDidEnterFullScreen:(NSNotification *)notification {
    self.rotationEnabled = YES;
}

- (void)videoPlayerDidExitFullScreen:(NSNotification *)notification {
    self.rotationEnabled = NO;
}

@end
