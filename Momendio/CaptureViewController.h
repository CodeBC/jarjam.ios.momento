//
//  CaptureViewController.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import "GPUImage.h"

@protocol CaptureViewControllerDelegate <NSObject>

- (void)captureView:(id)captureView didCaptureVideo:(Video*)video;

@end

@interface CaptureViewController : UIViewController

@property (weak, nonatomic) id<CaptureViewControllerDelegate> delegate;
@property (strong, nonatomic) Project *currentProject;

@end
