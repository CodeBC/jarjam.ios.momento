//
//  CaptureViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "CaptureViewController.h"
#import "SquareFilter.h"

#define kAccelerometerFrequency        10.0 //Hz

@interface CaptureViewController () <GPUImageMovieWriterDelegate,UIAccelerometerDelegate>

@property (weak, nonatomic) IBOutlet GPUImageView *previewView;
@property (weak, nonatomic) IBOutlet UIButton *captureButton;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *countLabel;
@property (weak, nonatomic) IBOutlet KAProgressLabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *topBarView;
@property (weak, nonatomic) IBOutlet UIView *bottomBarView;

@property (weak, nonatomic) IBOutlet UIView *rotateView;
@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UIView *switchCameraButton;

@property (strong, nonatomic) UIImageView *focusImageView;
@property (strong, nonatomic) NSTimer *focusTimer;

@property (strong, nonatomic) GPUImageVideoCamera *videoCamera;
@property (strong, nonatomic) GPUImageMovieWriter *movieWriter;
@property (strong, nonatomic) SquareFilter *squareFilter;
@property (strong, nonatomic) GPUImageBrightnessFilter *brightnessFilter;


- (IBAction)flashButtonPressed:(id)sender;
- (IBAction)switchCameraButtonPressed:(id)sender;
- (IBAction)captureVideoButtonPressed:(id)sender;
- (IBAction)closeButtonPressed:(id)sender;

@end

@implementation CaptureViewController
{
    UIInterfaceOrientation orientationLast;
}

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCaptureSessionError:) name:AVCaptureSessionRuntimeErrorNotification object:self];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSAssert(self.currentProject != nil, @"self.currentProject in CaptureViewController cannot be nil.");
    
    [self setStyling];
    [self updateUILabels];
    [self setupCamera];
    [self rotateLandscapeIfNeeded];
    [self resizePreviewLayerIfNeeded];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    if ([self isViewLoaded] && self.view.window == nil) {
        self.captureButton = nil;
        self.previewView = nil;
        self.countLabel = nil;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    // Start Monitor Orientation
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    UIAccelerometer* accelerometer = [UIAccelerometer sharedAccelerometer];
    accelerometer.updateInterval = 1 / kAccelerometerFrequency;
    accelerometer.delegate = self;
    
    [self setupGestures];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
    
    // Stop Monitoring Orientation
    UIAccelerometer* accelerometer = [UIAccelerometer sharedAccelerometer];
    accelerometer.delegate = nil;
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidDisappear:(BOOL)animated {
    [self closeCamera];
}


#pragma mark - Button Actions

- (IBAction)flashButtonPressed:(id)sender {
    DDLogVerbose(@"Torch");
    if ([self.videoCamera.inputCamera isTorchModeSupported:AVCaptureTorchModeOn] && [self.videoCamera.inputCamera lockForConfiguration:nil]) {
        
        if ([self.videoCamera.inputCamera isTorchActive]) {
            [self.videoCamera.inputCamera setTorchMode:AVCaptureTorchModeOff];
            [self.flashButton setImage:[UIImage imageNamed:@"flash-off"] forState:UIControlStateNormal];
        } else {
            [self.videoCamera.inputCamera setTorchMode:AVCaptureTorchModeOn];
            [self.flashButton setImage:[UIImage imageNamed:@"flash-on"] forState:UIControlStateNormal];
        }
        
        [self.videoCamera.inputCamera unlockForConfiguration];
    }
}

- (IBAction)switchCameraButtonPressed:(id)sender {
    DDLogVerbose(@"Switch Camera");
    
    // Off Torch if it's turned on
    if ([self.videoCamera.inputCamera isTorchActive]) {
        if ([self.videoCamera.inputCamera isTorchModeSupported:AVCaptureTorchModeOn] && [self.videoCamera.inputCamera lockForConfiguration:nil]) {
            [self.videoCamera.inputCamera setTorchMode:AVCaptureTorchModeOff];
            [self.flashButton setBackgroundImage:[UIImage imageNamed:@"flash-off"] forState:UIControlStateNormal];
            [self.videoCamera.inputCamera unlockForConfiguration];
        }
    }
    
    // If before switch is Back Camera
    if (self.videoCamera.cameraPosition == AVCaptureDevicePositionBack) {
        // Switch to 480p for support of older devices that can't do 720p
        if (!IS_IPHONE_5) { // Only iPhone 5 supports 720p front facing camera
            self.videoCamera.captureSessionPreset = AVCaptureSessionPreset640x480;
            
            if (self.squareFilter) { // If Cropping is used, update cropping ratio
                [self.squareFilter setCropFor480p];
            }
        }
        
        // Remove Torch Button since front cameras don't support torch
        self.flashButton.hidden = YES;
    }
    
    [self.videoCamera rotateCamera];
    
    // If after switch is Back Camera
    if (self.videoCamera.cameraPosition == AVCaptureDevicePositionBack) {
        // Switch to 720p for back camera
        self.videoCamera.captureSessionPreset = AVCaptureSessionPreset1280x720;
        
        if (self.squareFilter) { // If Cropping is used, update cropping ratio
            [self.squareFilter setCropFor720p];
        }
        
        // Re-add Torch Button if supported
        if ([self.videoCamera.inputCamera isTorchAvailable]) {
            self.flashButton.hidden = NO;
        }
        
        // Restore Camera Landscape
        if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationLandscape)]) {
            self.previewView.transform = CGAffineTransformIdentity;
        }
        
    } else {
        // Flip Video Feed if Front Camera Landscape
        if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationLandscape)]) {
            self.previewView.transform = CGAffineTransformMakeRotation(M_PI);
        }
    }
    
    
}

- (IBAction)captureVideoButtonPressed:(id)sender {
    
    if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        if (self.videoCamera.cameraPosition == AVCaptureDevicePositionFront && !IS_IPHONE_5) {
            // iPhone 4/4S supports 480p front facing camera only
                self.movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[self generateURL] size:CGSizeMake(480,480)];
        } else {
                self.movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[self generateURL] size:CGSizeMake(720,720)];
        }
    } else {
        if (self.videoCamera.cameraPosition == AVCaptureDevicePositionFront && !IS_IPHONE_5) {
            // iPhone 4/4S supports 480p front facing camera only
            self.movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[self generateURL] size:CGSizeMake(480,640)];
        } else {
            self.movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[self generateURL] size:CGSizeMake(720,1280)];
        }
    }
    
    if (self.squareFilter) {
        [self.squareFilter addTarget:self.movieWriter];
    } else {
        [self.videoCamera addTarget:self.movieWriter];
    }
    
    if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationLandscape)]) {
        // Need to flip if using front camera
        if (self.videoCamera.cameraPosition == AVCaptureDevicePositionFront) {
            [self.movieWriter startRecordingInOrientation:CGAffineTransformMakeRotation(M_PI_2)];
        } else {
            [self.movieWriter startRecordingInOrientation:CGAffineTransformMakeRotation(-M_PI_2)];
        }
    } else {
        [self.movieWriter startRecording];
    }
    
    self.movieWriter.delegate = self;
    [self.captureButton setEnabled:NO];
    
    [self.progressLabel setProgress:1.0 withAnimation:TPPropertyAnimationTimingLinear duration:[ProjectManager numberOfSecondsForProjectTemplate:self.currentProject.templateType.floatValue]-0.5 afterDelay:0.0f];
    
    [NSTimer bk_scheduledTimerWithTimeInterval:[ProjectManager numberOfSecondsForProjectTemplate:self.currentProject.templateType.floatValue] block:^(NSTimer *timer) {
        
        if (self.squareFilter) {
            [self.squareFilter removeTarget:self.movieWriter];

        } else {
            [self.videoCamera removeTarget:self.movieWriter];
        }
        
        [self.movieWriter finishRecording];
        
        
    } repeats:NO];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AVCaptureFileOutputRecordingDelegate

- (void)movieRecordingCompleted {
    DDLogVerbose(@"%@ %@",THIS_FILE,THIS_METHOD);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(captureView:didCaptureVideo:)]) {
        
            
        Video *newVideo = [Video createEntity];
        newVideo.localURLString = [self.movieWriter.assetWriter.outputURL absoluteString];
        
        if (self.videoCamera.cameraPosition == AVCaptureDevicePositionBack) {
            newVideo.cameraPosition = @(kVideoCameraSourceBackCamera);
        } else {
            newVideo.cameraPosition = @(kVideoCameraSourceFrontCamera);
        }
        
        UIImage *thumbnailImage = [UIImage thumbnailFromVideoAtURL:self.movieWriter.assetWriter.outputURL];
        if (thumbnailImage) {
            newVideo.thumbnailData = UIImageJPEGRepresentation(thumbnailImage,0.5);
        }
        
        [self.delegate captureView:self didCaptureVideo:newVideo];
    }
    
    [self updateUILabels];
    [self.captureButton setEnabled:YES];
    [self.progressLabel setProgress:0.0f];
}

- (void)movieRecordingFailedWithError:(NSError*)error {
    DDLogVerbose(@"%@: %@, value: %@",THIS_FILE,THIS_METHOD,error);
    
        [self.progressLabel setProgress:0.0f];
        [self.captureButton setEnabled:YES];
}

#pragma mark - Helper Functions

- (void)setStyling {
    
    if (!IS_IPHONE_5) {
        [self.switchCameraButton setHidden:YES];
    }
    
    [self.countLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:20.0f]];
    [self.progressLabel setBorderWidth:4.0f];
    [self.progressLabel setFillColor:[UIColor clearColor]];
    [self.progressLabel setTrackColor:[UIColor whiteColor]];
    [self.progressLabel setProgressColor:[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    
    self.focusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"focus"]];
    
}

- (void)setupCamera {
    
    self.previewView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    
    self.videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset1280x720 cameraPosition:AVCaptureDevicePositionBack];
    
    if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        self.squareFilter = [SquareFilter new];
        [self.videoCamera addTarget:self.squareFilter];
        [self.squareFilter addTarget:self.previewView];
        
    } else {
        self.brightnessFilter = [GPUImageBrightnessFilter new];
        [self.videoCamera addTarget:self.brightnessFilter];
        [self.brightnessFilter addTarget:self.previewView];
    }
    
    self.videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    self.videoCamera.horizontallyMirrorFrontFacingCamera = NO;
    self.videoCamera.horizontallyMirrorRearFacingCamera = NO;
    
    [self.videoCamera startCameraCapture];
}

- (void)closeCamera {
    [self.videoCamera stopCameraCapture];
    self.videoCamera = nil;
}

- (void)setupGestures {
    
    // Point Focus on Tap
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        double screenWidth = screenRect.size.width;
        double screenHeight = screenRect.size.height;
        double focus_x = location.x/screenWidth;
        double focus_y = location.y/screenHeight;
        CGPoint center = location;
        
        // Convert location from portrait coordinates to landscape coordinates for detecting taps.
        if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationLandscape)]) {
            CGFloat tempX = location.x;
            location.x = location.y;
            location.y = 320.0f - tempX;
        }
        
        NSLog(@"Tapped at %f %f",location.x,location.y);
        NSLog(@"Control Bars at %f %f %f %f, %f %f %f %f",self.topBarView.frame.origin.x,self.topBarView.frame.origin.y,self.topBarView.frame.size.width,self.topBarView.frame.size.height,self.bottomBarView.frame.origin.x,self.bottomBarView.frame.origin.y,self.bottomBarView.frame.size.width,self.bottomBarView.frame.size.height);
        
        if ( CGRectContainsPoint(self.topBarView.frame, location) || CGRectContainsPoint(self.bottomBarView.frame, location) ) {
            // Skip handler if tapped control bars
            NSLog(@"Ignore due to tapped on control bars.");
            return;
        } else if ( !CGRectContainsPoint(self.previewView.frame, location) &&  [self.currentProject.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
            // Skip if tapped outside preview layer (applicable for square video)
            return;
        }
        
        NSError *error = nil;
        DDLogVerbose(@"%@",self.videoCamera.inputCamera);
        
        if ([self.videoCamera.inputCamera isFocusModeSupported:AVCaptureFocusModeAutoFocus] && [self.videoCamera.inputCamera lockForConfiguration:&error]) {
            DDLogVerbose(@"Locking Focus");
            [self.videoCamera.inputCamera setFocusMode:AVCaptureFocusModeAutoFocus];
            [self.videoCamera.inputCamera setFocusPointOfInterest:CGPointMake(focus_x,focus_y)];
            [self.videoCamera.inputCamera unlockForConfiguration];
            
            [self.focusImageView removeFromSuperview];
            [self.view addSubview:self.focusImageView];
            self.focusImageView.center = center;
            
            if (self.focusTimer) {
                [self.focusTimer invalidate];
                self.focusTimer = nil;
            }
            
            self.focusTimer = [NSTimer bk_scheduledTimerWithTimeInterval:3.0f block:^(NSTimer *timer) {
                [self.focusImageView removeFromSuperview];
            } repeats:NO];
            
        } else {
            DDLogError(@"Lock Error: %@",error);
        }
    }];
    tapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
    // Auto Focus on Double Tap
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        [self.focusImageView removeFromSuperview];
        
        DDLogVerbose(@"Auto Focus");
        if ([self.videoCamera.inputCamera isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus] && [self.videoCamera.inputCamera lockForConfiguration:nil]) {
            CGPoint autofocusPoint = CGPointMake(0.5f, 0.5f);
            [self.videoCamera.inputCamera setFocusPointOfInterest:autofocusPoint];
            [self.videoCamera.inputCamera setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
            [self.videoCamera.inputCamera unlockForConfiguration];
        }
        
        if (self.focusTimer) {
            [self.focusTimer invalidate];
            self.focusTimer = nil;
        }
    }];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
}

- (void)updateUILabels {
    
    NSString *countLabelString = [NSString stringWithFormat:@"%d/%d",self.currentProject.videos.count,[ProjectManager numberOfClipsForProjectTemplate:[self.currentProject.templateType intValue]]];
    
    [self.countLabel setText:countLabelString afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^([0-9]*)"
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];

        NSArray *matches = [regex matchesInString:countLabelString
                                          options:0
                                            range:NSMakeRange(0, [countLabelString length])];

        for (NSTextCheckingResult *checkingResult in matches) {

            UIFont *mentionFont = [UIFont fontWithName:@"Avenir-Light" size:20.0];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)mentionFont.fontName, mentionFont.pointSize, NULL);

            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(id)CFBridgingRelease(font) range:checkingResult.range];
            }

            CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
            CGFloat components[] = { 84.0f/255.0f, 179.0f/255.0f, 169.0f/255.0f, 1.0 };
            CGColorRef red = CGColorCreate(rgbColorSpace, components);
            CGColorSpaceRelease(rgbColorSpace);
            CFAttributedStringSetAttribute((__bridge CFMutableAttributedStringRef)(mutableAttributedString), CFRangeMake(checkingResult.range.location, checkingResult.range.length),
                                           kCTForegroundColorAttributeName, red);
        }
        return mutableAttributedString;
    }];
    
    if (self.currentProject.videos.count == [ProjectManager numberOfClipsForProjectTemplate:[self.currentProject.templateType intValue]]) {
        [SVProgressHUD showSuccessWithStatus:@"All Clips Taken"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (NSURL *)generateURL {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MM-yyyy-HH-mm-ss"];
    
    NSString *fileURLString = [[ProjectManager getDocsDirectory] stringByAppendingFormat:@"/%@.mov",[dateFormatter stringFromDate:[NSDate date]]];
    
    NSURL *fileURL = [NSURL fileURLWithPath:fileURLString];
    
    return fileURL;
}

- (void)rotateLandscapeIfNeeded {
    if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationLandscape)]) {
        
        if (IS_IPHONE_5) {
            [[self overlayView] setFrame:CGRectMake(0, 0, 568, 320)];
            [[self overlayView] setCenter:CGPointMake(160, 284)];
        } else {
            [[self overlayView] setFrame:CGRectMake(0, 0, 480, 320)];
            [[self overlayView] setCenter:CGPointMake(160, 240)];
        }
        
        [[self overlayView] setTransform:CGAffineTransformMakeRotation(M_PI_2)];
    }
}

- (void)resizePreviewLayerIfNeeded {
    if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        
        if (IS_IPHONE_5) {
            [[self previewView] setFrame:CGRectMake(0, 0, 320, 320)];
            [[self previewView] setCenter:CGPointMake(160, 284)];
        } else {
            [[self previewView] setFrame:CGRectMake(0, 0, 320, 320)];
            [[self previewView] setCenter:CGPointMake(160, 220)];
        }

    }
}

#pragma mark - Notification Handlers

- (void)handleCaptureSessionError: (NSNotification *)notification {
    DDLogVerbose(@"%@: %@, %@",THIS_FILE,THIS_METHOD,notification);
}

+(NSString*)orientationToText:(const UIInterfaceOrientation)ORIENTATION {
    switch (ORIENTATION) {
        case UIInterfaceOrientationPortrait:
            return @"UIInterfaceOrientationPortrait";
        case UIInterfaceOrientationPortraitUpsideDown:
            return @"UIInterfaceOrientationPortraitUpsideDown";
        case UIInterfaceOrientationLandscapeLeft:
            return @"UIInterfaceOrientationLandscapeLeft";
        case UIInterfaceOrientationLandscapeRight:
            return @"UIInterfaceOrientationLandscapeRight";
    }
    return @"Unknown orientation!";
}

#pragma mark UIAccelerometerDelegate

-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    UIInterfaceOrientation orientationNew;
    if (acceleration.x >= 0.75) {
        orientationNew = UIInterfaceOrientationLandscapeLeft;
    }
    else if (acceleration.x <= -0.75) {
        orientationNew = UIInterfaceOrientationLandscapeRight;
    }
    else if (acceleration.y <= -0.75) {
        orientationNew = UIInterfaceOrientationPortrait;
    }
    else if (acceleration.y >= 0.75) {
        orientationNew = UIInterfaceOrientationPortraitUpsideDown;
    }
    else {
        // Consider same as last time
        return;
    }
    
    if (orientationNew == orientationLast)
        return;
    
    DDLogVerbose(@"Going from %@ to %@!", [[self class] orientationToText:orientationLast], [[self class] orientationToText:orientationNew]);
    orientationLast = orientationNew;
    
    if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [self.currentProject.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        if (orientationNew == UIInterfaceOrientationPortrait) {
            self.rotateView.hidden = YES;
        } else {
            self.rotateView.hidden = NO;
            [self.movieWriter cancelRecording];
        }
    } else {
        if (orientationNew == UIInterfaceOrientationLandscapeRight) {
            self.rotateView.hidden = YES;
        } else {
            self.rotateView.hidden = NO;
            [self.movieWriter cancelRecording];
        }
    }
}


@end
