//
//  ChooseOrientationViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 9/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ChooseOrientationViewController.h"

@interface ChooseOrientationViewController ()

@property (strong, nonatomic) IBOutlet UILabel *chooseOrientationLabel;

- (IBAction)portraitButtonPressed:(id)sender;
- (IBAction)landscapeButtonPressed:(id)sender;
- (IBAction)squareButtonPressed:(id)sender;

- (IBAction)orientationButtonTouchDown:(id)sender;
- (IBAction)orientationButtonTouchDragOutside:(id)sender;

- (IBAction)settingsButtonPressed:(id)sender;
- (IBAction)galleryButtonPressed:(id)sender;

@end

@implementation ChooseOrientationViewController

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (IBAction)portraitButtonPressed:(UIButton *)button {
    [ProjectManager sharedInstance].projectOrientation = kProjectOrientationPortrait;
    [self performSegueWithIdentifier:@"ChooseOrientationToProject" sender:self];
    
    button.layer.borderWidth = 0.0f;
}

- (IBAction)landscapeButtonPressed:(UIButton *)button {
    [ProjectManager sharedInstance].projectOrientation = kProjectOrientationLandscape;
    [self performSegueWithIdentifier:@"ChooseOrientationToProject" sender:self];
    
    button.layer.borderWidth = 0.0f;
}

- (IBAction)squareButtonPressed:(UIButton *)button {
    [ProjectManager sharedInstance].projectOrientation = kProjectOrientationSquare;
    [self performSegueWithIdentifier:@"ChooseOrientationToProject" sender:self];
    
    button.layer.borderWidth = 0.0f;
}

- (IBAction)orientationButtonTouchDown:(UIButton *)button {
    button.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
    button.layer.borderWidth = 0.5f;
}

- (IBAction)orientationButtonTouchDragOutside:(UIButton *)button {
    button.layer.borderWidth = 0.0f;
}

- (IBAction)settingsButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ChooseOrientationToSettings" sender:self];
}

- (IBAction)galleryButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ChooseOrientationToGallery" sender:self];
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Helper Functions

- (void)setStyling {
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    backButton.tintColor = [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.chooseOrientationLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:16.0f]];
}

@end
