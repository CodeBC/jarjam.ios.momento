//
//  ChooseTemplateViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 9/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ChooseTemplateViewController.h"

@interface ChooseTemplateViewController ()

@property (strong, nonatomic) IBOutlet UILabel *chooseTemplateLabel;
@property (strong, nonatomic) IBOutlet UILabel *letsShootLabel;

- (IBAction)templateButtonPressed:(id)sender;
- (IBAction)galleryButtonPressed:(id)sender;
- (IBAction)settingsButtonPressed:(id)sender;

- (IBAction)buttonTouchDown:(id)sender;
- (IBAction)buttonCancelTouch:(id)sender;

@end

@implementation ChooseTemplateViewController

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (IBAction)templateButtonPressed:(UIButton *)button {
    // Buttons identified via tag.
    // Eg. 24 Clips is Button with tag 24 etc.
    
    switch (button.tag) {
        case 24:
            [ProjectManager sharedInstance].projectTemplate = kProjectTemplate24ClipsBy1Secs;
            break;
            
        case 12:
            [ProjectManager sharedInstance].projectTemplate = kProjectTemplate12ClipsBy2Secs;
            break;
            
        case 8:
            [ProjectManager sharedInstance].projectTemplate = kProjectTemplate8ClipsBy3Secs;
            break;
            
        default:
            [ProjectManager sharedInstance].projectTemplate = kProjectTemplate24ClipsBy1Secs;
            break;
    }
    
    [self performSegueWithIdentifier:@"ChooseTemplateToChooseOrientation" sender:self];
    
    button.layer.borderWidth = 0.0f;
}

- (IBAction)buttonTouchDown:(UIButton *)button {
    button.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
    button.layer.borderWidth = 0.5f;
}

- (IBAction)buttonCancelTouch:(UIButton *)button {
    button.layer.borderWidth = 0.0f;
}

- (IBAction)galleryButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ChooseTemplateToGallery" sender:self];
}

- (IBAction)settingsButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ChooseTemplateToSettings" sender:self];
}

#pragma mark - Helper Functions

- (void)setStyling {
    [self.chooseTemplateLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:16.0f]];
    [self.letsShootLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:15.0f]];
    
}

@end
