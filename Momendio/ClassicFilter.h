//
//  ClassicFilter.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 13/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GPUImageFilterGroup.h"

@interface ClassicFilter : GPUImageFilterGroup
{
    GPUImageSaturationFilter *grayscaleFilter;
    GPUImageContrastFilter *constrastFilter;
}


@end
