//
//  ClassicFilter.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 13/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ClassicFilter.h"

@implementation ClassicFilter

- (id)init
{
    if (!(self = [super init]))
    {
		return nil;
    }

    grayscaleFilter = [[GPUImageSaturationFilter alloc] init];
    grayscaleFilter.saturation = 0.0f;
    [self addFilter:grayscaleFilter];
    
    constrastFilter = [[GPUImageContrastFilter alloc] init];
    constrastFilter.contrast = 4.0;
    
    [grayscaleFilter addTarget:constrastFilter];
    
    self.initialFilters = @[grayscaleFilter];
    self.terminalFilter = constrastFilter;
    
    return self;
}

@end
