//
//  Constants.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 9/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <SystemConfiguration/SystemConfiguration.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "AFNetworking.h"

#import <BlocksKit.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import <BlocksKit/MFMailComposeViewController+BlocksKit.h>
#import <BlocksKit/UIAlertView+BlocksKit.h>
#import <BlocksKit/NSTimer+BlocksKit.h>
#import <BlocksKit/UIGestureRecognizer+BlocksKit.h>

#import "DAKeyboardControl.h"

//If you want to omit the "MR_" prefix to all MagicalRecord realted method calls define following before importing the MagicalRecord header
#define MR_SHORTHAND

//Turn off MagicalRecord logging, again by defining following before header import
//#define MR_ENABLE_ACTIVE_RECORD_LOGGING 0

#import "CoreData+MagicalRecord.h"
#import "SVProgressHUD.h"
#import "ABFlatSwitch.h"
#import "TTTAttributedLabel.h"
#import "KAProgressLabel.h"

#import "DDlog.h"
#import "DDTTYLogger.h"

#import "ProjectManager.h"
#import "FilterManager.h"
#import "MusicManager.h"

#import "Project.h"
#import "Video.h"

// Category to generate thumbnails
#import "UIImage+AVThumbnail.h"

// Log levels: off, error, warn, info, verbose
#if DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_INFO;
#endif

// Detect iPhone 5
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

// Enums
typedef NS_ENUM(NSUInteger, kFontType) {
    kFontTypeFunctionPro,
    kFontTypeHex,
    kFontTypeKGSomebody,
    kFontTypeLaBelleAurore,
    kFontTypeSearsTower,
    kFontTypeUnna,
    kFontTypeCount
};

typedef NS_ENUM(NSUInteger, kColorType) {
    kColorTypeWhite,
    kColorTypeBlack,
    kColorTypeRed,
    kColorTypeYellow,
    kColorTypeBrown,
    kColorTypeCount
};

// Facebook Constants
#define FacebookDidLoginSuccess @"FacebookDidLoginSuccess"
#define FacebookDidLoginFail @"FacebookDidLoginFail"

// YouTube Constants
extern NSString *const kYouTubeClientID;
extern NSString *const kYouTubeClientSecret;
extern NSString *const kYouTubeKeychainItemName;
extern NSString *const kYouTubeScope;

// Tutorial Constants
extern NSString *const kTutorialDefaultskey;

// Text

#define ABOUT_TEXT @"Momendio is an easy and fun way to capture and share life’s moments on video with friends and family. Create bite-size 24 second videos using Momendio’s simple and flexible sequence. Give them life by transforming the look and feel with designer filters and perfectly timed music that seamlessly syncs to it. The results are beautiful, memorable movies of life. Share your moments in Facebook, YouTube and more.\n\nFor more information please visit www.momendio.com"

@interface Constants : NSObject

+ (UIFont *)fontForFontType:(kFontType)fontType atSize:(CGFloat)fontSize;
+ (UIColor *)colorForColorType:(kColorType)colorType;

@end
