//
//  Constants.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 9/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString *const kYouTubeClientID = @"353457024417.apps.googleusercontent.com";
NSString *const kYouTubeClientSecret = @"g0jGjED_Qe75pjjXgYd9KDZ1";
NSString *const kYouTubeKeychainItemName = @"Momendio:YouTube";
NSString *const kYouTubeScope = @"https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/userinfo.email";

NSString *const kTutorialDefaultskey = @"kTutorialDefaultskey";

+ (UIFont *)fontForFontType:(kFontType)fontType atSize:(CGFloat)fontSize {
    
    UIFont *font = nil;
    
    switch(fontType) {
        case kFontTypeFunctionPro:
            font = [UIFont fontWithName:@"Function Pro" size:fontSize];
            break;
            
        case kFontTypeHex:
            font = [UIFont fontWithName:@"Ampersand" size:fontSize];
            break;
            
        case kFontTypeKGSomebody:
            font = [UIFont fontWithName:@"KG Somebody That I Used to Know" size:fontSize];
            break;
            
        case kFontTypeLaBelleAurore:
            font = [UIFont fontWithName:@"La Belle Aurore" size:fontSize];
            break;
            
        case kFontTypeSearsTower:
            font = [UIFont fontWithName:@"Franchise" size:fontSize];
            break;
            
        case kFontTypeUnna:
            font = [UIFont fontWithName:@"Unna" size:fontSize];
            break;
            
        default:
            font = nil;
    }
    
    return font;
}

+ (UIColor *)colorForColorType:(kColorType)colorType {
    
    UIColor *color = nil;
    
    switch (colorType) {
        case kColorTypeBlack:
            color = [UIColor blackColor];
            break;
            
        case kColorTypeWhite:
            color = [UIColor whiteColor];
            break;
            
        case kColorTypeRed:
            color = [UIColor colorWithRed:0.99 green:0.05 blue:0.11 alpha:1];
            break;
            
        case kColorTypeYellow:
            color = [UIColor colorWithRed:1.0 green:1.0 blue:0.27 alpha:1];
            break;
            
        case kColorTypeBrown:
            color = [UIColor colorWithRed:0.35 green:0.06 blue:0.09 alpha:1];
            break;
            
        default:
            color = nil;
            
    }
    
    return color;
}

@end
