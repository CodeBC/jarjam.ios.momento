//
//  CreditsViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "CreditsViewController.h"

#import "ShareViewController.h"
#import "UIPlaceHolderTextView.h"

@interface CreditsViewController () <UITextViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIPlaceHolderTextView *titleTextView;
@property (strong, nonatomic) IBOutlet UITextField *authorTextField;
@property (strong, nonatomic) IBOutlet UIView *publishButtonDesign;

- (IBAction)dismissKeyboardButtonPressed:(id)sender;
- (IBAction)publishButtonPressed:(id)sender;
- (IBAction)publishButtonTouchDown:(id)sender;
- (IBAction)publishButtonDragOutside:(id)sender;

@end

@implementation CreditsViewController

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];
    [self populateData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dismissKeyboardButtonPressed:(id)sender {
    [self.titleTextView resignFirstResponder];
    [self.authorTextField resignFirstResponder];
}

- (IBAction)publishButtonPressed:(id)sender {
    
    if (self.titleTextView.text.length == 0 || self.authorTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"Some fields are empty"];
        return;
    }
    
    [self dismissKeyboardButtonPressed:nil];
    
    UIAlertView *alertView = [UIAlertView bk_alertViewWithTitle:@"Publish Project" message:@"Are your sure?"];
    
    [alertView bk_addButtonWithTitle:@"Cancel" handler:^{
        self.publishButtonDesign.layer.borderWidth = 0.0f;
    }];
    
    [alertView bk_addButtonWithTitle:@"Publish" handler:^{
        
        self.publishButtonDesign.layer.borderWidth = 0.0f;
        
        [SVProgressHUD showWithStatus:@"Processing Video..." maskType:SVProgressHUDMaskTypeGradient];
        
        if (self.titleTextView.text.length)
            self.currentProject.name = self.titleTextView.text;
        
        if (self.authorTextField.text.length)
            self.currentProject.author = self.authorTextField.text;
        
        [ProjectManager publishProject:self.currentProject withCompletion:^(NSURL *publishedURL, NSError *error, NSInteger currentCount, BOOL isDone) {
            
            if (isDone) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (publishedURL) {
                        
                        Project *publishedProject = [Project createEntity];
                        
                        publishedProject.templateType = self.currentProject.templateType;
                        publishedProject.orientation = self.currentProject.orientation;
                        publishedProject.musicLocalURLString = self.currentProject.musicLocalURLString;
                        publishedProject.musicType = self.currentProject.musicType;
                        publishedProject.musicVolume = self.currentProject.musicVolume;
                        
                        publishedProject.name = self.currentProject.name;
                        publishedProject.author = self.currentProject.author;
                        publishedProject.about = self.currentProject.about;
                        
                        publishedProject.published = @YES;
                        publishedProject.publishedLocalURLString = [publishedURL absoluteString];
                        
                        [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
                        [SVProgressHUD showSuccessWithStatus:@"Published!"];
                        [self performSegueWithIdentifier:@"CreditsToShare" sender:publishedProject];
                        
                    } else {
                        DDLogError(@"%@: %@, Error: %@",THIS_FILE,THIS_METHOD,[error localizedDescription]);
                        [SVProgressHUD showErrorWithStatus:@"Error occurred!"];
                    }
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    CGFloat currentProgress = [@(currentCount) floatValue];
                    CGFloat totalProgress = [@(self.currentProject.videos.count) floatValue];
                    CGFloat progress = currentProgress/totalProgress;
                    NSString *progressString = [NSString stringWithFormat:@"Filtering Video (%d/%d)",currentCount,self.currentProject.videos.count];
                    [SVProgressHUD showProgress:progress status:progressString maskType:SVProgressHUDMaskTypeGradient];
                });
            }
            
        }];
        
    }];
    [alertView show];
    
}

- (IBAction)publishButtonTouchDown:(id)sender {
    self.publishButtonDesign.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
    self.publishButtonDesign.layer.borderWidth = 0.5f;
}

- (IBAction)publishButtonDragOutside:(id)sender {
    self.publishButtonDesign.layer.borderWidth = 0.0f;
}

#pragma mark - Prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CreditsToShare"]) {
        
        Project *publishedProject = (Project *)sender;
        
        ShareViewController *shareView = segue.destinationViewController;
        shareView.currentProject = publishedProject;
    }
}

#pragma mark - Textfield Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Limits Textfield length to 60 chars
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 40) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    // If return key pressed, move to author textfield
    if ([text isEqualToString:@"\n"]) {
        [self.authorTextField becomeFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    
    // Limits Textfield length to 60 chars
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    return (newLength > 50) ? NO : YES;
}

#pragma mark - Helper Functions

- (void)setStyling {
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.titleTextView.placeholder = @"Enter Title";
    self.titleTextView.placeholderColor = [UIColor grayColor];
    
    self.titleTextView.delegate = self;
}

- (void)populateData {
    if (self.currentProject) {
        self.titleTextView.text = self.currentProject.name;
        self.authorTextField.text = self.currentProject.author;
    }
}


@end
