//
//  EditOverlayViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "EditOverlayViewController.h"

@interface EditOverlayViewController ()

- (IBAction)dismissButtonPressed:(id)sender;

@end

@implementation EditOverlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (IBAction)dismissButtonPressed:(id)sender {
    [self.view removeFromSuperview];
}

@end
