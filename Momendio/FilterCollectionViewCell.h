//
//  FilterCollectionViewCell.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 11/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

@interface FilterCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *filterImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

- (void)setStyling;

@end
