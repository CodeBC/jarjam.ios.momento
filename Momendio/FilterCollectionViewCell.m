//
//  FilterCollectionViewCell.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 11/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FilterCollectionViewCell.h"

@implementation FilterCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setStyling {
    [self.nameLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:13.0f]];
}

@end
