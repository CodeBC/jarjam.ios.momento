//
//  FilterManager.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 11/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPUImage.h"

typedef NS_ENUM(NSUInteger, kVideoFilter) {
    kVideoFilterNone,
    kVideoFilterClassic,
    kVideoFilterToast,
    kVideoFilter1958,
    kVideoFilter1976,
    kVideoFilterHiLo,
    kVideoFilterSunset,
    kVideoFilterHampton,
    kVideoFilterTwilight,
    kVideoFilterRainbow,
    kVideoFilterCount
};

@interface FilterManager : NSObject

+ (FilterManager *)sharedInstance;

+ (NSString *)filterNameForFilterType:(kVideoFilter)filter;
+ (UIImage *)filterSampleImageForFilterType:(kVideoFilter)filter;

+ (UIImage *)filteredImageFromImage:(UIImage *)originalImage withFilter:(kVideoFilter)filter;
+ (GPUImageOutput<GPUImageInput> *)filterObjectForFilterType:(kVideoFilter)filter;

- (void)filterVideoAtURL:(NSURL *)url withFilter:(kVideoFilter)filterType andCompletion:(void(^)(NSURL *filteredURL, NSError *error))completion;

@end