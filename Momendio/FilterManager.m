//
//  FilterManager.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 11/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FilterManager.h"

#import "HamptonFilter.h"
#import "RainbowFilter.h"
#import "ToastFilter.h"
#import "SunsetFilter.h"
#import "TwilightFilter.h"
#import "HiLoFilter.h"
#import "1958Filter.h"
#import "1976Filter.h"
#import "ClassicFilter.h"

#import "SquareFilter.h"

@interface FilterManager ()

@property (strong, nonatomic) GPUImageMovie *movieFile;
@property (strong, nonatomic) GPUImageOutput<GPUImageInput> *filter;
@property (strong, nonatomic) GPUImageMovieWriter *movieWriter;

@end

@implementation FilterManager

@synthesize movieFile;
@synthesize filter;
@synthesize movieWriter;

+ (id)sharedInstance
{
    static FilterManager *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[FilterManager alloc] init];
    });
    
    return __sharedInstance;
}

+ (NSString *)filterNameForFilterType:(kVideoFilter)filter {
    switch (filter) {
            
        case kVideoFilterNone:
            return @"No Filter";
            break;
            
        case kVideoFilterClassic:
            return @"Classic";
            break;
            
        case kVideoFilterToast:
            return @"Toast";
            break;
            
        case kVideoFilter1958:
            return @"1958";
            break;
            
        case kVideoFilter1976:
            return @"1976";
            break;
            
        case kVideoFilterHiLo:
            return @"Urban";
            break;
            
        case kVideoFilterSunset:
            return @"Sunset";
            break;
            
        case kVideoFilterHampton:
            return @"Hampton";
            break;
            
        case kVideoFilterTwilight:
            return @"Twilight";
            break;
            
        case kVideoFilterRainbow:
            return @"Rainbow";
            break;
            
        default:
            return nil;
            break;
    }
}

+ (UIImage *)filterSampleImageForFilterType:(kVideoFilter)filter {
    
    switch (filter) {
            
        case kVideoFilterNone:
            return [UIImage imageNamed:@"filterSample.jpg"];
            break;
            
        case kVideoFilterClassic:
            return [UIImage imageNamed:@"Pinwheel_Classic"];
            break;
            
        case kVideoFilterToast:
            return [UIImage imageNamed:@"Pinwheel_Toast"];
            break;
            
        case kVideoFilter1958:
            return [UIImage imageNamed:@"Pinwheel_1958"];
            break;
            
        case kVideoFilter1976:
            return [UIImage imageNamed:@"Pinwheel_1976"];
            break;
            
        case kVideoFilterHiLo:
            return [UIImage imageNamed:@"Pinwheel_Urban"];;
            break;
            
        case kVideoFilterSunset:
            return [UIImage imageNamed:@"Pinwheel_Sunset"];;
            break;
            
        case kVideoFilterHampton:
            return [UIImage imageNamed:@"Pinwheel_Hampton"];;
            break;
            
        case kVideoFilterTwilight:
            return [UIImage imageNamed:@"Pinwheel_Twilight"];;
            break;
            
        case kVideoFilterRainbow:
            return [UIImage imageNamed:@"Pinwheel_Rainbow"];
            break;
            
        default:
            return nil;
            break;
    }
}

+ (UIImage *)filteredImageFromImage:(UIImage *)originalImage withFilter:(kVideoFilter)filter {
    
    if (filter == kVideoFilterNone) {
        return originalImage;
    }
    
    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:originalImage];
    GPUImageOutput<GPUImageInput> *stillImageFilter = [self filterObjectForFilterType:filter];
    
    [stillImageSource addTarget:stillImageFilter];
//    [stillImageSource processImage];
    return [stillImageFilter imageByFilteringImage:originalImage];
}

- (void)filterVideoAtURL:(NSURL *)url withFilter:(kVideoFilter)filterType andCompletion:(void(^)(NSURL *filteredURL, NSError *error))completion {
    
    filter = [self.class filterObjectForFilterType:filterType];
    if (filter == nil) { // It could be no filter applied.
        // Send back original videoURL if video not filtered.
        if (completion) completion(url,nil);
        return;
    }
    
    // Original Video & Filter
    movieFile = [[GPUImageMovie alloc] initWithURL:url];
//    movieFile.runBenchmark = YES;
    movieFile.playAtActualSpeed = NO; // Process as fast as possible
  
    [movieFile addTarget:filter];
    
    // Generate Filtered URL
    NSString *oldFileNameWithoutExtension = [[[url absoluteString] lastPathComponent] stringByDeletingPathExtension];
    NSString *newFileName = [NSString stringWithFormat:@"%@-filtered.mov",oldFileNameWithoutExtension];
    
    NSString *pathToMovie = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",newFileName];
    DDLogVerbose(@"%@:%@, %@",THIS_FILE,THIS_METHOD,pathToMovie);
    unlink([pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
    NSURL *movieURL = [[NSURL alloc] initFileURLWithPath:pathToMovie];
    
    // Get Asset's Track to obtain size
    AVURLAsset *movieAsset = [AVURLAsset URLAssetWithURL:url options:@{AVURLAssetPreferPreciseDurationAndTimingKey:@YES}];
    AVAssetTrack *videoTrack = [[movieAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    
    CGSize preferredSize = videoTrack.naturalSize;
    
    DDLogVerbose(@"%d w%f h%f",[FilterManager orientationForTrack:movieAsset],preferredSize.width,preferredSize.height);
    
    // Movie Writer
    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:movieURL size:preferredSize];
    [filter addTarget:movieWriter];
    [movieFile enableSynchronizedEncodingUsingMovieWriter:movieWriter];
    
    __weak typeof(self) weakSelf = self;
    [movieWriter setCompletionBlock:^{
        [weakSelf.filter removeTarget:weakSelf.movieWriter];
        [weakSelf.movieWriter finishRecording];
        weakSelf.movieWriter = nil;
        weakSelf.filter = nil;
        
        DDLogVerbose(@"Filter Completion Block Called");
        
        if (completion) completion(movieURL,nil);
    }];
    
    [movieWriter startRecordingInOrientation:videoTrack.preferredTransform];
    [movieFile startProcessing];

}

#pragma mark - Helper Functions

+ (GPUImageOutput<GPUImageInput> *)filterObjectForFilterType:(kVideoFilter)filter {
    switch (filter) {
            
        case kVideoFilterNone:
            return nil;//return [GPUImageBrightnessFilter new];
            break;

        case kVideoFilterClassic:
            return [ClassicFilter new];
            break;
            
        case kVideoFilterToast:
            return [ToastFilter new];
            break;
            
        case kVideoFilter1958:
            return [_958Filter new];
            break;
            
        case kVideoFilter1976:
            return [_976Filter new];
            break;
            
        case kVideoFilterHiLo:
            return [HiLoFilter new];
            break;
            
        case kVideoFilterSunset:
            return [SunsetFilter new];
            break;
            
        case kVideoFilterHampton:
            return [HamptonFilter new];
            break;
            
        case kVideoFilterTwilight:
            return [TwilightFilter new];
            break;
            
        case kVideoFilterRainbow:
            return [RainbowFilter new];
            break;
        
        default:
            return nil;
            break;
    }
}

+ (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}

@end
