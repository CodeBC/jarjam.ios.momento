//
//  FilterOverlayViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 13/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FilterOverlayViewController.h"

@interface FilterOverlayViewController ()

- (IBAction)dismissButtonPressed:(id)sender;

@end

@implementation FilterOverlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissButtonPressed:(id)sender {
    [self.view removeFromSuperview];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kTutorialDefaultskey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
