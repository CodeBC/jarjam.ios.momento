//
//  FilterTextViewController.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 2/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterTextViewController : UIViewController

@property (strong, nonatomic) Project *currentProject;

@end
