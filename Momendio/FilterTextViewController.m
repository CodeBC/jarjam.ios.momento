//
//  FilterTextViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 2/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FilterTextViewController.h"
#import "FilterManager.h"

#import "FilterCollectionViewCell.h"
#import "MusicViewController.h"
#import "AppDelegate.h"

@interface FilterTextViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) NSMutableArray *videoArray;
@property (strong, nonatomic) NSMutableArray *thumbnailImageViews;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *pageLabel;
@property (assign, nonatomic) NSInteger currentPage;

@property (assign, nonatomic) CGFloat currentFontSize;

@property (weak, nonatomic) IBOutlet UICollectionView *filterCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *colorCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *fontTableView;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (strong, nonatomic) IBOutlet UIButton *filterButton;
@property (strong, nonatomic) IBOutlet UIButton *textButton;
@property (strong, nonatomic) IBOutlet UITextField *inputTextField;

- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)filterButtonPressed:(id)sender;
- (IBAction)textButtonPressed:(id)sender;

@end

@implementation FilterTextViewController

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];
    [self updateVideoArray];
    [self setupScrollView];
    [self setupTextFieldPanGesture];
    [self setupTextFieldPinchGesture];
}

- (void)setupTextFieldPinchGesture {
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [self.view addGestureRecognizer:pinchGesture];
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)pinchGesture {
    self.currentFontSize *= pinchGesture.scale;
    self.currentFontSize = MAX(self.currentFontSize, 14.0f);
    self.currentFontSize = MIN(self.currentFontSize, 40.0f);
    self.inputTextField.font = [Constants fontForFontType:self.currentProject.textFontType.intValue atSize:self.currentFontSize];
    self.currentProject.fontSize = @(self.currentFontSize);
}

- (void)setupTextFieldPanGesture {
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.inputTextField addGestureRecognizer:panGesture];
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)panGesture {
    
    // Assume Gesture Point is Center of new frame.
    CGPoint gesturePoint = [panGesture locationInView:self.view];
    
    NSString *inputString = @"";
    if (self.inputTextField.text.length > 0) {
        inputString = self.inputTextField.text;
    } else {
        // Simulate the placeholder text
        inputString = @"TAP ANYWHERE TO EDIT TEXT";
    }
    
    // Get Bounds and Sizes
    CGRect previewBounds = self.scrollView.frame;
    CGRect textFieldFrame = self.inputTextField.frame;
    CGFloat inputTextWidth = [inputString sizeWithFont:[Constants fontForFontType:self.currentProject.textFontType.intValue atSize:self.currentFontSize] constrainedToSize:textFieldFrame.size].width;
    
    CGFloat padding = 20.0f;
    
    // Check if gesture center is beyond bounds else max/min it.
    CGFloat newCenterX = MAX(gesturePoint.x, padding + inputTextWidth/2); // Check Left Bounds
    newCenterX = MIN(newCenterX, previewBounds.size.width - padding - inputTextWidth/2); // Check Right Bounds
    
    CGFloat newCenterY = MAX(gesturePoint.y, padding + textFieldFrame.size.height/2); // Check Top Bounds
    newCenterY = MIN(newCenterY, previewBounds.size.height - (padding * 2) - textFieldFrame.size.height/2); // Check Bottom Bounds
    
    self.inputTextField.center = CGPointMake(newCenterX, newCenterY);
    
    self.currentProject.textOffsetX = @(self.inputTextField.center.x - self.scrollView.center.x);
    self.currentProject.textOffsetY = @(self.inputTextField.center.y - self.scrollView.center.y);
}

- (void)viewWillAppear:(BOOL)animated {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate showFilterOverlayIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.view removeKeyboardControl];
}

#pragma mark - Button Actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneButtonPressed:(id)sender {
    
    if (self.inputTextField.text.length > 0) {
        self.currentProject.textString = self.inputTextField.text;
    } else {
        self.currentProject.textString = nil;
    }
    
    [self performSegueWithIdentifier:@"FilterTextToMusic" sender:self];
}

- (IBAction)filterButtonPressed:(id)sender {
    [self.filterButton setImage:[UIImage imageNamed:@"btn_fx_selected"] forState:UIControlStateNormal];
    [self.textButton setImage:[UIImage imageNamed:@"btn_text"] forState:UIControlStateNormal];
    
    self.filterCollectionView.hidden = NO;
    self.colorCollectionView.hidden = YES;
    self.fontTableView.hidden = YES;
    self.inputTextField.hidden = YES;
    
    [self.view bringSubviewToFront:self.filterCollectionView];
}

- (IBAction)textButtonPressed:(id)sender {
    [self.filterButton setImage:[UIImage imageNamed:@"btn_fx"] forState:UIControlStateNormal];
    [self.textButton setImage:[UIImage imageNamed:@"btn_text_selected"] forState:UIControlStateNormal];
    
    self.filterCollectionView.hidden = YES;
    self.colorCollectionView.hidden = NO;
    self.fontTableView.hidden = NO;
    self.inputTextField.hidden = NO;
    
    [self.view bringSubviewToFront:self.colorCollectionView];
    [self.view bringSubviewToFront:self.fontTableView];
}

#pragma mark - Prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"FilterTextToMusic"]) {
        MusicViewController *musicView = segue.destinationViewController;
        musicView.currentProject = self.currentProject;
    } 
}

#pragma mark - PSTCollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    if (view == self.filterCollectionView) {
        return kVideoFilterCount;
    } else {
        return kColorTypeCount;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (cv == self.filterCollectionView) {
        
        FilterCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"FilterCollectionViewCell" forIndexPath:indexPath];
        [cell setStyling];
        cell.nameLabel.text = [FilterManager filterNameForFilterType:indexPath.row];
        cell.filterImageView.image = [FilterManager filterSampleImageForFilterType:indexPath.row];
        cell.filterImageView.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
        
        // Highlight Current Applied Filter
        Video *currentVideo = self.videoArray[self.currentPage-1];
        if (currentVideo.filterType.intValue == indexPath.row) { // Current Applied Filter
            cell.filterImageView.layer.borderWidth = 2.0f;
        } else {
            cell.filterImageView.layer.borderWidth = 0.0f;
        }
        
        UITapGestureRecognizer *doubleTapFolderGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(processDoubleTap:)];
        [doubleTapFolderGesture setNumberOfTapsRequired:2];
        [doubleTapFolderGesture setNumberOfTouchesRequired:1];
        [cell addGestureRecognizer:doubleTapFolderGesture];
        
        return cell;
        
    } else {
        
        UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ColorCell" forIndexPath:indexPath];
        
        UIColor *color = [Constants colorForColorType:indexPath.row];
        
        if (color) {
            cell.backgroundColor = color;
        }
        
        return cell;
        
    }
}

- (void)processDoubleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [sender locationInView:self.filterCollectionView];
        NSIndexPath *indexPath = [self.filterCollectionView indexPathForItemAtPoint:point];
        
        if (indexPath) {
            NSLog(@"Filter was double tapped");
            
            [self.videoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Video *currentVideo = self.videoArray[idx];
                currentVideo.filterType = @(indexPath.row);
                [self applyFilterToImageViewAtIndex:idx withVideo:currentVideo];
            }];

        }
        
    }
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.filterCollectionView) {
        
        Video *currentVideo = self.videoArray[self.currentPage-1];

        currentVideo.filterType = @(indexPath.row);
        [self applyFilterToImageViewAtIndex:self.currentPage-1 withVideo:currentVideo];
        [self.filterCollectionView reloadData];
        
    } else {
        
        UIColor *color = [Constants colorForColorType:indexPath.row];
        
        if (color) {
            
            // Update Placeholder Color
            if ([self.inputTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                self.inputTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.inputTextField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            }
            
            self.inputTextField.textColor = color;
            self.currentProject.textColorType = @(indexPath.row);
        }
        
    }
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.filterCollectionView) {
        return CGSizeMake(70, 90);
    } else {
        return CGSizeMake(40, 40);
    }
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.filterCollectionView) {
        return UIEdgeInsetsMake(10, 10, 10, 10);
    } else {
        return UIEdgeInsetsMake(0,0,0,0);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.filterCollectionView) {
        return 5;
    } else {
        return 0;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.filterCollectionView) {
        return 5;
    } else {
        return 0;
    }
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return kFontTypeCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FontCell" forIndexPath:indexPath];
    
    NSString *fontname;
    UIFont *font;
    switch(indexPath.row) {
        case kFontTypeFunctionPro:
            fontname = @"Function Pro";
            font = [UIFont fontWithName:@"Function Pro" size:18.0f];
            break;
            
        case kFontTypeHex:
            fontname = @"Ampersand";
            font = [UIFont fontWithName:@"Ampersand" size:18.0f];
            break;
            
        case kFontTypeKGSomebody:
            fontname = @"KG Somebody";
            font = [UIFont fontWithName:@"KG Somebody That I Used to Know" size:18.0f];
            break;
            
        case kFontTypeLaBelleAurore:
            fontname = @"La Belle Aurore";
            font = [UIFont fontWithName:@"La Belle Aurore" size:18.0f];
            break;
            
        case kFontTypeSearsTower:
            fontname = @"Franchise";
            font = [UIFont fontWithName:@"Franchise" size:18.0f];
            break;
            
        case kFontTypeUnna:
            fontname = @"Unna";
            font = [UIFont fontWithName:@"Unna" size:18.0f];
            break;
            
        default:
            fontname = @"Unknown";
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor colorWithRed:0.11 green:0.73 blue:0.7 alpha:1];
    cell.textLabel.text = fontname;
    
    if (font) {
        cell.textLabel.font = font;
    }
    
    return cell;
}

#pragma mark - TableView Delegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIFont *font = [Constants fontForFontType:indexPath.row atSize:self.currentFontSize];
    
    if (font) {
        self.inputTextField.font = font;
        NSLog(@"%@", self.inputTextField.font);
        self.currentProject.textFontType = @(indexPath.row);
    }
    
}

#pragma mark - Helper Functions

- (void)setStyling
{
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.doneButton setBackgroundImage:[[UIImage alloc]init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [self.filterCollectionView registerNib:[UINib nibWithNibName:@"FilterCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"FilterCollectionViewCell"];
    
    self.colorCollectionView.hidden = YES;
    self.fontTableView.hidden = YES;
    self.inputTextField.hidden = YES;
    
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView) {
        
    }];
    
    if (self.currentProject.textString) {
        self.inputTextField.text = self.currentProject.textString;
    }
    
    if (!self.currentProject.textColorType) {
        self.currentProject.textColorType = @0; // Default Color
    }
    
    if (!self.currentProject.textFontType) {
        self.currentProject.textFontType = @0; // Default Font
    }
    
    if (self.currentProject.fontSize.floatValue < 10.0f) {
        self.currentProject.fontSize = @18.0f;
        self.currentFontSize = 18.0f;
    } else {
        self.currentFontSize = self.currentProject.fontSize.floatValue;
    }
    
    UIColor *textColor = [Constants colorForColorType:self.currentProject.textColorType.intValue];
    if ([self.inputTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        self.inputTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.inputTextField.placeholder attributes:@{NSForegroundColorAttributeName: textColor}];
    }
    self.inputTextField.textColor = textColor;
    
    self.inputTextField.font = [Constants fontForFontType:self.currentProject.textFontType.intValue atSize:self.currentFontSize];
    self.inputTextField.center = CGPointMake(self.scrollView.center.x + self.currentProject.textOffsetX.floatValue, self.scrollView.center.y + self.currentProject.textOffsetY.floatValue);
}

- (void)setupScrollView
{
    self.thumbnailImageViews = [NSMutableArray new];
    NSInteger totalPages = self.videoArray.count;
    NSInteger pageIndex = 0;
    self.scrollView.contentSize = CGSizeMake(320.0f*totalPages, 1);
    for (Video *video in self.videoArray) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.layer.masksToBounds = YES;
        
        if (IS_IPHONE_5) {
            [imageView setFrame:CGRectMake(pageIndex*320.0f + 10.0f, 10.0f, 300.0f, 356.0f-40.0f)];
        } else {
            [imageView setFrame:CGRectMake(pageIndex*320.0f + 10.0f, 10.0f, 300.0f, 268.0f-40.0f)];
        }
        
        [self.scrollView addSubview:imageView];
        [self.thumbnailImageViews addObject:imageView];
        [self applyFilterToImageViewAtIndex:pageIndex withVideo:video];
        pageIndex++;
    }
    
    self.currentPage = 1;
    self.pageLabel.text = [NSString stringWithFormat:@"1/%d",totalPages];
}

- (void)updateVideoArray
{
    if (self.currentProject && self.currentProject.videos.count) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];
        self.videoArray = [[self.currentProject.videos sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    } else {
        self.videoArray = [@[] mutableCopy];
    }
    
    [self.filterCollectionView reloadData];
}

- (void)applyFilterToImageViewAtIndex:(NSInteger)index withVideo:(Video *)video
{
    UIImage *inputImage = [UIImage imageWithData:video.thumbnailData];
    if (!inputImage) {
        [video refreshThumbnail];
        inputImage = [UIImage imageWithData:video.thumbnailData];
    }
    
    UIImageView *imageView = self.thumbnailImageViews[index];
    GPUImageOutput *filter = [FilterManager filterObjectForFilterType:[video.filterType intValue]];
    if (filter) {
        [imageView setImage:[filter imageByFilteringImage:inputImage]];
    } else {
        [imageView setImage:inputImage];
    }
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 2;
    self.pageLabel.text = [NSString stringWithFormat:@"%d/%d",page,self.videoArray.count];
    
    BOOL changePage = self.currentPage != page;
    self.currentPage = page;
    
    if (changePage) { // Change of page
        [self.filterCollectionView reloadData];
        Video *currentVideo = self.videoArray[self.currentPage-1];
        [self.filterCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:currentVideo.filterType.intValue inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
