//
//  GalleryViewController.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kGalleryModeVideos,
    kGalleryModeProjects
} kGalleryMode;

@interface GalleryViewController : UIViewController

@end
