//
//  GalleryViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GalleryViewController.h"

#import "ShareViewController.h"
#import "ProjectViewController.h"
#import "VideoCollectionViewCell.h"
#import "VideoTableViewCell.h"

@interface GalleryViewController ()

@property (strong, nonatomic) NSMutableArray *projectsArray;
@property (strong, nonatomic) NSMutableArray *publishedArray;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UILabel *projectsLabel;
@property (strong, nonatomic) IBOutlet UILabel *videosLabel;

@property (assign, nonatomic) kGalleryMode galleryMode;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;


- (IBAction)projectsButtonPressed:(id)sender;
- (IBAction)videosButtonPressed:(id)sender;
- (IBAction)addButtonPressed:(id)sender;

@end

@implementation GalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];
    [self setupTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [self setupDataSources];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)projectsButtonPressed:(id)sender {
    self.galleryMode = kGalleryModeProjects;
    [self.tableView reloadData];
    
    self.projectsLabel.textColor = [UIColor colorWithRed:0.0f green:177.0/255.0 blue:177.0/255.0 alpha:1.0f];
    self.videosLabel.textColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    [self.projectsLabel setFont:[UIFont fontWithName:@"Avenir-Heavy" size:17.0f]];
    [self.videosLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:17.0f]];
}

- (IBAction)videosButtonPressed:(id)sender {
    self.galleryMode = kGalleryModeVideos;
    [self.tableView reloadData];
    
    self.projectsLabel.textColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    self.videosLabel.textColor = [UIColor colorWithRed:0.0f green:177.0/255.0 blue:177.0/255.0 alpha:1.0f];
    [self.projectsLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:17.0f]];
    [self.videosLabel setFont:[UIFont fontWithName:@"Avenir-Heavy" size:17.0f]];
}

- (IBAction)addButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.publishedArray && self.galleryMode == kGalleryModeVideos) {
        return self.publishedArray.count;
    } else if (self.projectsArray && self.galleryMode == kGalleryModeProjects) {
        return self.projectsArray.count;
    } else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 == 1) { // Odd Rows
        cell.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:30.0f/255.0f green:30.0f/255.0f blue:30.0f/255.0f alpha:1.0];
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoCell"];
    [cell setStyling];
    
    Project *cellProject;
    if (self.galleryMode == kGalleryModeVideos) {
        cellProject = self.publishedArray[indexPath.row];
    } else {
        cellProject = self.projectsArray[indexPath.row];
    }
    
    cell.nameLabel.text = (cellProject.name.length) ? cellProject.name : @"No Title";

    if (cellProject.published.boolValue) {
        cell.previewImageView.image = [UIImage thumbnailFromVideoAtURL:[NSURL URLWithString:[cellProject getRelativePublishedURL]]];
    } else if (cellProject.videos.count) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];
        NSArray *videoArray = [cellProject.videos sortedArrayUsingDescriptors:@[sortDescriptor]];
        Video *firstVideo = videoArray[0];
        cell.previewImageView.image = [UIImage imageWithData:firstVideo.thumbnailData];
    }
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
    // Delete the row from the data source
        
        if (self.galleryMode == kGalleryModeVideos) {
            [ProjectManager deleteProject:self.publishedArray[indexPath.row]];
            [self.publishedArray removeObjectAtIndex:indexPath.row];
        } else {
            [ProjectManager deleteProject:self.projectsArray[indexPath.row]];
            [self.projectsArray removeObjectAtIndex:indexPath.row];
        }
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.galleryMode == kGalleryModeVideos) {
        ShareViewController *shareView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareView"];
        shareView.currentProject = self.publishedArray[indexPath.row];
        [self.navigationController pushViewController:shareView animated:YES];
    } else {
        ProjectViewController *projectView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectView"];
        projectView.currentProject = self.projectsArray[indexPath.row];
        [self.navigationController pushViewController:projectView animated:YES];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Helper Functions

- (void)setStyling {
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.addButton setBackgroundImage:[[UIImage alloc]init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    self.videosLabel.textColor = [UIColor colorWithRed:0.0f green:177.0/255 blue:177.0/255 alpha:1.0f];
    [self.videosLabel setFont:[UIFont fontWithName:@"Avenir-Heavy" size:17.0f]];
    [self.projectsLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:17.0f]];
}

- (void)setupTableView {
    UINib *videoCellNib = [UINib nibWithNibName:@"VideoTableViewCell" bundle:nil];
    [self.tableView registerNib:videoCellNib forCellReuseIdentifier:@"VideoCell"];
}

- (void)setupDataSources {
    self.projectsArray = [[Project findByAttribute:@"published" withValue:nil] mutableCopy];
    self.publishedArray = [[Project findByAttribute:@"published" withValue:@YES] mutableCopy];
    self.galleryMode = kGalleryModeVideos;
    [self.tableView reloadData];
}

@end
