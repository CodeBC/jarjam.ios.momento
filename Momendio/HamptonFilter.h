//
//  HamptonFilter.h
//  Momendio
//
//  Created by Nur Iman Izam on 23/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GPUImageFilter.h"

@interface HamptonFilter : GPUImageFilterGroup
{
    GPUImagePicture *lookupImageSource;
}

@end
