//
//  HiLoFilter.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 11/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GPUImageFilterGroup.h"

@interface HiLoFilter : GPUImageFilterGroup
{
    GPUImagePicture *lookupImageSource;
}

@end
