//
//  LoginViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 22/10/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "LoginViewController.h"
#import "YouTubeController.h"
#import "AppDelegate.h"
#import "APIClient.h"

#import <FacebookSDK/FacebookSDK.h>

@interface LoginViewController ()

- (IBAction)facebookButtonPressed:(id)sender;
- (IBAction)googleButtonPressed:(id)sender;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookLoginSuccess) name:FacebookDidLoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookLoginFailed) name:FacebookDidLoginFail object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Facebook Login

- (IBAction)facebookButtonPressed:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate openFacebookSession];
}

- (void)facebookLoginSuccess {
    
    [SVProgressHUD showWithStatus:@"Logging you in..." maskType:SVProgressHUDMaskTypeGradient];
    
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        DDLogVerbose(@"%@",result);
        
        if (!error) {
        
            NSDictionary *params = @{@"user": @{ @"name": result[@"name"],
                                                 @"email": result[@"email"],
                                                 @"password": result[@"id"],
                                                 @"confirmation": result[@"id"] }
                                     };
            
            [[APIClient sharedInstance] postPath:@"api/v1/users" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSError *jsonError = nil;
                NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&jsonError];
                
                if (!error) {
                    DDLogVerbose(@"%@ %@: %@",THIS_FILE,THIS_METHOD,response);
                    
                }
                
                [SVProgressHUD showSuccessWithStatus:@"Logged in!"];
                [self switchToLoggedInView];
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DDLogVerbose(@"%@ %@: Error %@",THIS_FILE,THIS_METHOD,error);
                [SVProgressHUD showErrorWithStatus:@"Error logging you in."];
            }];
            
        } else {
            DDLogVerbose(@"%@ %@: Error %@",THIS_FILE,THIS_METHOD,error);
            [SVProgressHUD showErrorWithStatus:@"Error logging you in."];
        }
        
    }];
}

- (void)facebookLoginFailed {
    [SVProgressHUD showErrorWithStatus:@"Failed to login Facebook."];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Google Login

- (IBAction)googleButtonPressed:(id)sender {
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    UIViewController *loginView = [[YouTubeController sharedInstance] loginViewControllerWithCompletionHandler:^(NSError *error) {
        if (!error) {
            [SVProgressHUD showWithStatus:@"Logging you in..." maskType:SVProgressHUDMaskTypeGradient];
            
            NSString *googleProfilePath = [NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=%@",[[YouTubeController sharedInstance].auth accessToken]];
            [[APIClient sharedInstance] getPath:googleProfilePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSError *jsonError = nil;
                NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&jsonError];
                
                if (!error) {
                    DDLogVerbose(@"%@ %@: %@",THIS_FILE,THIS_METHOD,response);
                    
                    NSDictionary *params = @{@"user": @{ @"name": response[@"email"],
                                                         @"email": response[@"email"],
                                                         @"password": response[@"email"],
                                                         @"confirmation": response[@"email"] }
                                             };
                    
                    [[APIClient sharedInstance] postPath:@"api/v1/users" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        
                        NSError *jsonError = nil;
                        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&jsonError];
                        
                        if (!error) {
                            DDLogVerbose(@"%@ %@: %@",THIS_FILE,THIS_METHOD,response);
                        }
                        
                        [SVProgressHUD showSuccessWithStatus:@"Logged in!"];
                        [self switchToLoggedInView];
                        
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        DDLogVerbose(@"%@ %@: Error %@",THIS_FILE,THIS_METHOD,error);
                        [SVProgressHUD showErrorWithStatus:@"Error logging you in."];
                    }];
                    
                } else {
                    [SVProgressHUD showErrorWithStatus:@"Error logging you in."];
                }
                
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DDLogVerbose(@"%@ %@: Error %@",THIS_FILE,THIS_METHOD,error);
                [SVProgressHUD showErrorWithStatus:@"Error logging you in."];
            }];
            
        } else {
            if ([error code] != -1000) {
                [SVProgressHUD dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            } else {
                // Login Cancelled
            }
        }
    }];
    
    [self.navigationController pushViewController:loginView animated:YES];
    
}

#pragma mark - Helper Functions

- (void)switchToLoggedInView {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *loggedinViewController = [storyboard instantiateViewControllerWithIdentifier:@"MomendioNavigation"];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.window.rootViewController presentViewController:loggedinViewController animated:YES completion:nil];
}

@end
