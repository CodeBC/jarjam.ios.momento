//
//  MailViewController.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 27/10/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <MessageUI/MessageUI.h>

@interface MailViewController : MFMailComposeViewController

@end
