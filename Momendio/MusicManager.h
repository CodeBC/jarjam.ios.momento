//
//  MusicManager.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 13/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kMusicTypeAfternoonLullaby,
    kMusicTypeChillLounge,
    kMusicTypeGetaway,
    kMusicTypeHualalai,
    kMusicTypeMorningBreeze,
    kMusicTypeRockConcert,
    kMusicTypeSmoothGrove,
    kMusicTypeSweethearts,
    kMusicTypeWonderment,
    kMusicTypeAfter8,
    kMusicTypeEnchanted,
    kMusicTypeRipples,
    kMusicTypeSouthshoreJive,
    kMusicTypeVega,
    kMusicTypeMojitoSwing,
    kMusicTypeMoonlightStroll,
    kMusicTypeSunsetCabana,
    
    // Serendipity
    kMusicTypeAngelCity,
    kMusicTypeEmbrace,
    kMusicTypeGravity,
    kMusicTypeSendingMyLove,
    kMusicTypeShipwrecks
    
} kMusicType;

@interface MusicManager : NSObject

+ (MusicManager *)sharedInstance;

+ (NSString *)musicNameForMusicType:(kMusicType)music;
+ (NSString *)filePathForMusicType:(kMusicType)music;
@end
