//
//  MusicManager.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 13/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "MusicManager.h"

@implementation MusicManager

+ (id)sharedInstance
{
    static MusicManager *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[MusicManager alloc] init];
    });
    
    return __sharedInstance;
}

+ (NSString *)musicNameForMusicType:(kMusicType)music {
    switch (music) {
            
        case kMusicTypeAfternoonLullaby:
            return @"Afternoon Lullaby";
            break;
            
        case kMusicTypeChillLounge:
            return @"Chill Lounge";
            break;
            
        case kMusicTypeGetaway:
            return @"Getaway";
            break;
            
        case kMusicTypeHualalai:
            return @"Hualalai";
            break;
            
        case kMusicTypeMorningBreeze:
            return @"Morning Breeze";
            break;
            
        case kMusicTypeRockConcert:
            return @"Rock Concert";
            break;
            
        case kMusicTypeSmoothGrove:
            return @"Smooth Grove";
            break;
            
        case kMusicTypeSweethearts:
            return @"Sweethearts";
            break;
            
        case kMusicTypeWonderment:
            return @"Wonderment 3";
            break;
            
        case kMusicTypeAfter8:
            return @"After 8";
            break;
            
        case kMusicTypeEnchanted:
            return @"Enchanted";
            break;
            
        case kMusicTypeRipples:
            return @"Ripples";
            break;
            
        case kMusicTypeSouthshoreJive:
            return @"Southshore Jive";
            break;
            
        case kMusicTypeVega:
            return @"Vega";
            break;
            
        case kMusicTypeMojitoSwing:
            return @"Mojito Swing";
            break;
            
        case kMusicTypeMoonlightStroll:
            return @"Moonlight Stroll";
            break;
            
        case kMusicTypeSunsetCabana:
            return @"Sunset Cabana";
            break;
            
        case kMusicTypeAngelCity:
            return @"Angel City";
            break;
            
        case kMusicTypeEmbrace:
            return @"Embrace";
            break;
            
        case kMusicTypeGravity:
            return @"Gravity";
            break;
            
        case kMusicTypeSendingMyLove:
            return @"Sending My Love";
            break;
            
        case kMusicTypeShipwrecks:
            return @"Shipwrecks";
            break;
            
        default:
            return @"Unknown";
            break;
            
    }
}

+ (NSString *)filePathForMusicType:(kMusicType)music {
    switch (music) {
        case kMusicTypeAfternoonLullaby:
            return [[NSBundle mainBundle] pathForResource:@"AfternoonLullaby" ofType:@"mp3"];
            break;
            
        case kMusicTypeChillLounge:
            return [[NSBundle mainBundle] pathForResource:@"ChillLounge" ofType:@"mp3"];
            break;
            
        case kMusicTypeGetaway:
            return [[NSBundle mainBundle] pathForResource:@"Getaway" ofType:@"mp3"];
            break;
            
        case kMusicTypeHualalai:
            return [[NSBundle mainBundle] pathForResource:@"Hualalai" ofType:@"mp3"];
            break;
            
        case kMusicTypeMorningBreeze:
            return [[NSBundle mainBundle] pathForResource:@"MorningBreeze" ofType:@"mp3"];
            break;
            
        case kMusicTypeRockConcert:
            return [[NSBundle mainBundle] pathForResource:@"RockConcert" ofType:@"mp3"];
            break;
            
        case kMusicTypeSmoothGrove:
            return [[NSBundle mainBundle] pathForResource:@"SmoothGrove" ofType:@"mp3"];
            break;
            
        case kMusicTypeSweethearts:
            return [[NSBundle mainBundle] pathForResource:@"Sweethearts" ofType:@"mp3"];
            break;
            
        case kMusicTypeWonderment:
            return [[NSBundle mainBundle] pathForResource:@"Wonderment3" ofType:@"mp3"];
            break;
            
        case kMusicTypeAfter8:
            return [[NSBundle mainBundle] pathForResource:@"After8" ofType:@"mp3"];
            break;
            
        case kMusicTypeEnchanted:
            return [[NSBundle mainBundle] pathForResource:@"Enchanted" ofType:@"mp3"];
            break;
            
        case kMusicTypeRipples:
            return [[NSBundle mainBundle] pathForResource:@"Ripples" ofType:@"mp3"];
            break;
            
        case kMusicTypeSouthshoreJive:
            return [[NSBundle mainBundle] pathForResource:@"SouthshoreJive" ofType:@"mp3"];
            break;
            
        case kMusicTypeVega:
            return [[NSBundle mainBundle] pathForResource:@"Vega" ofType:@"mp3"];
            break;
            
        case kMusicTypeMojitoSwing:
            return [[NSBundle mainBundle] pathForResource:@"MojitoSwing" ofType:@"mp3"];
            break;
            
        case kMusicTypeMoonlightStroll:
            return [[NSBundle mainBundle] pathForResource:@"MoonlightStroll" ofType:@"mp3"];
            break;
            
        case kMusicTypeSunsetCabana:
            return [[NSBundle mainBundle] pathForResource:@"SunsetCabana" ofType:@"mp3"];
            break;
            
        case kMusicTypeAngelCity:
            return [[NSBundle mainBundle] pathForResource:@"AngelCity" ofType:@"mp3"];
            break;
            
        case kMusicTypeEmbrace:
            return [[NSBundle mainBundle] pathForResource:@"Embrace" ofType:@"mp3"];
            break;
            
        case kMusicTypeGravity:
            return [[NSBundle mainBundle] pathForResource:@"Gravity" ofType:@"mp3"];
            break;
            
        case kMusicTypeSendingMyLove:
            return [[NSBundle mainBundle] pathForResource:@"SendingMyLove" ofType:@"mp3"];
            break;
            
        case kMusicTypeShipwrecks:
            return [[NSBundle mainBundle] pathForResource:@"Shipwrecks" ofType:@"mp3"];
            break;
            
        default:
            return nil;
            break;
    }
}

@end
