//
//  MusicTableViewCell.h
//  Momendio
//
//  Created by Nur Iman Izam on 3/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusicTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIView *innerView;

- (void)setStyling;

@end
