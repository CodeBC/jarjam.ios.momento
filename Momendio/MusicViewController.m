//
//  MusicViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "MusicViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "PSTCollectionView.h"

#import "AlbumCollectionViewCell.h"

#import "CreditsViewController.h"
#import "MusicTableViewCell.h"
#import "AlbumTableViewCell.h"

@interface MusicViewController () <AVAudioPlayerDelegate, PSTCollectionViewDataSource,PSTCollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;


@property (strong, nonatomic) IBOutlet PSTCollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *albumHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *genreHeaderLabel;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (strong, nonatomic) NSArray *freeAlbumArray;
@property (strong, nonatomic) NSArray *montaggioAlbumArray;
@property (strong, nonatomic) NSArray *paradisoAlbumArray;
@property (strong, nonatomic) NSArray *serendipityAlbumArray;

@property (assign, nonatomic) NSInteger selectedAlbumIndex;

- (IBAction)doneButtonPressed:(id)sender;

@end

@implementation MusicViewController

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupAlbumData];
    [self setStyling];
    [self setupDefaultSelection];
    [self setupAudioPlayBackIfPossible];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.audioPlayer stop];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneButtonPressed:(id)sender {
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
    [self performSegueWithIdentifier:@"MusicToCredits" sender:self];
}

#pragma mark - Prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"MusicToCredits"]) {
        CreditsViewController *creditsView = segue.destinationViewController;
        creditsView.currentProject = self.currentProject;
    }
}

#pragma mark - PSTCollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView: (PSTCollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(PSTCollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return kAlbumTypeCount;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AlbumCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"AlbumCollectionViewCell" forIndexPath:indexPath];
    [cell setStyling];
    
    cell.albumImageView.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
    
    cell.albumNameLabel.text = [self albumNameForAlbumType:indexPath.row];
    cell.albumGenreLabel.text = [self albumGenreForAlbumType:indexPath.row];
    cell.albumImageView.image = [self albumCoverForAlbumType:indexPath.row];
    
    if (indexPath.row == self.selectedAlbumIndex) {
        cell.albumImageView.layer.borderWidth = 2.0f;
    } else {
        cell.albumImageView.layer.borderWidth = 0.0f;
    }
    
    return cell;
}

#pragma mark – PSTCollectionViewDelegateFlowLayout

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 140);
}

- (UIEdgeInsets)collectionView:
(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 20, 0, 20);
}

- (CGFloat)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

- (CGFloat)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

- (void)collectionView:(PSTCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedAlbumIndex = indexPath.row;
    [self.collectionView reloadData];
    [self.tableView reloadData];
    
    self.albumHeaderLabel.text = [[self albumNameForAlbumType:self.selectedAlbumIndex] uppercaseString];
    self.genreHeaderLabel.text = [self albumGenreForAlbumType:self.selectedAlbumIndex];
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.selectedAlbumIndex == kAlbumTypeFree) {
        return self.freeAlbumArray.count;
    } else if (self.selectedAlbumIndex == kAlbumTypeMontaggio) {
        return self.montaggioAlbumArray.count;
    } else if (self.selectedAlbumIndex == kAlbumTypeParadiso) {
        return self.paradisoAlbumArray.count;
    } else if (self.selectedAlbumIndex == kAlbumTypeSerendipity) {
        return self.serendipityAlbumArray.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MusicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MusicCell"];
    [cell setStyling];
    cell.innerView.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
    
    if (self.selectedAlbumIndex == kAlbumTypeFree) {
        
        cell.nameLabel.text = [MusicManager musicNameForMusicType:[self.freeAlbumArray[indexPath.row] intValue]];
        
    } else if (self.selectedAlbumIndex == kAlbumTypeMontaggio) { // kAlbumTypeMontaggio

        cell.nameLabel.text = [MusicManager musicNameForMusicType:[self.montaggioAlbumArray[indexPath.row] intValue]];
        
    } else if (self.selectedAlbumIndex == kAlbumTypeParadiso) {
        
        cell.nameLabel.text = [MusicManager musicNameForMusicType:[self.paradisoAlbumArray[indexPath.row] intValue]];
        
    } else {
        
        cell.nameLabel.text = [MusicManager musicNameForMusicType:[self.serendipityAlbumArray[indexPath.row] intValue]];
        
    }
    
    // Selection Highlight
    if (indexPath.row == self.selectedIndexPath.row) {
        cell.innerView.layer.borderWidth = 2.0f;
    } else {
        cell.innerView.layer.borderWidth = 0.0f;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    kMusicType musicType;
    
    if (self.selectedAlbumIndex == kAlbumTypeFree) {
        
        musicType = [self.freeAlbumArray[indexPath.row] intValue];
        
    } else if (self.selectedAlbumIndex == kAlbumTypeMontaggio) { // kAlbumTypeMontaggio
        
        musicType = [self.montaggioAlbumArray[indexPath.row] intValue];
    
    } else if (self.selectedAlbumIndex == kAlbumTypeParadiso) { // Paradiso Album
        
        musicType = [self.paradisoAlbumArray[indexPath.row] intValue];
            
    } else { // Serendipity Album
        
        musicType = [self.serendipityAlbumArray[indexPath.row] intValue];
        
    }
    
    // Set Music
    self.currentProject.musicType = @(musicType);
    self.currentProject.musicVolume = @1.0;
    self.currentProject.musicLocalURLString = [MusicManager filePathForMusicType:musicType];
    
    self.selectedIndexPath = indexPath;
    [self.tableView reloadData];
    
    NSError *error = nil;
    [self.audioPlayer stop];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:[MusicManager filePathForMusicType:musicType]] error:&error];
    
    DDLogVerbose(@"URL: %@",self.audioPlayer.url);
    
    [self.audioPlayer setDelegate:self];
    
    if (error) {
        DDLogError(@"%@: %@, Error: %@",THIS_FILE,THIS_METHOD,[error localizedDescription]);
    } else {
        [self.audioPlayer play];
    }
    
}

#pragma mark - Helper Functions

- (void)setupAlbumData {
    self.freeAlbumArray = @[@(kMusicTypeChillLounge),@(kMusicTypeGetaway),@(kMusicTypeMorningBreeze),@(kMusicTypeSmoothGrove),@(kMusicTypeSweethearts)];
    self.montaggioAlbumArray = @[@(kMusicTypeAfter8),@(kMusicTypeEnchanted),@(kMusicTypeRipples),@(kMusicTypeSouthshoreJive),@(kMusicTypeVega)];
    self.paradisoAlbumArray = @[@(kMusicTypeAfternoonLullaby),@(kMusicTypeHualalai),@(kMusicTypeMojitoSwing),@(kMusicTypeMoonlightStroll),@(kMusicTypeSunsetCabana)];
    self.serendipityAlbumArray = @[@(kMusicTypeAngelCity),@(kMusicTypeEmbrace),@(kMusicTypeGravity),@(kMusicTypeSendingMyLove),@(kMusicTypeShipwrecks)];
}

- (void)setStyling {
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.doneButton setBackgroundImage:[[UIImage alloc]init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;

    [self.collectionView registerNib:[UINib nibWithNibName:@"AlbumCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AlbumCollectionViewCell"];
    
    PSTCollectionViewFlowLayout *flowLayout = [PSTCollectionViewFlowLayout new];
    flowLayout.scrollDirection = PSTCollectionViewScrollDirectionHorizontal;
    [self.collectionView setCollectionViewLayout:flowLayout animated:YES];
    
    [self.albumHeaderLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:17.0f]];
    [self.genreHeaderLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:15.0f]];

}

- (void)setupDefaultSelection {
    if (self.currentProject.musicType) {
        self.selectedIndexPath = [self mapMusicType:[self.currentProject.musicType intValue]];
        self.selectedAlbumIndex = self.selectedIndexPath.section;
    } else {
        self.selectedAlbumIndex = kAlbumTypeFree;
        self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:kAlbumTypeFree];
        self.currentProject.musicType = @0;
        self.currentProject.musicVolume = @1.0;
        self.currentProject.musicLocalURLString = [MusicManager filePathForMusicType:[self.freeAlbumArray[0] intValue]];
    }
    
    self.albumHeaderLabel.text = [[self albumNameForAlbumType:self.selectedAlbumIndex] uppercaseString];
    self.genreHeaderLabel.text = [self albumGenreForAlbumType:self.selectedAlbumIndex];
}

- (NSString *)albumNameForAlbumType:(kAlbumType)albumType {
    
    switch (albumType) {
        case kAlbumTypeFree:
            return @"Footloose";
        case kAlbumTypeMontaggio:
            return @"Montaggio";
        case kAlbumTypeParadiso:
            return @"Paradiso";
        case kAlbumTypeSerendipity:
            return @"Serendipity";
            
        default:
            return @"";
    }
}

- (NSString *)albumGenreForAlbumType:(kAlbumType)albumType {
    
    switch (albumType) {
        case kAlbumTypeFree:
            return @"Mixed Genre";
        case kAlbumTypeMontaggio:
            return @"Mixed Genre";
        case kAlbumTypeParadiso:
            return @"Resort Collection";
        case kAlbumTypeSerendipity:
            return @"Sentimental";
            
        default:
            return @"";
    }
}

- (UIImage *)albumCoverForAlbumType:(kAlbumType)albumType {
    
    switch (albumType) {
        case kAlbumTypeFree:
            return [UIImage imageNamed:@"albumcover_free.jpg"];
            
        case kAlbumTypeMontaggio:
            return [UIImage imageNamed:@"album-montaggio.jpg"];
            
        case kAlbumTypeParadiso:
            return [UIImage imageNamed:@"albumcover_paradiso.jpg"];
            break;
            
        case kAlbumTypeSerendipity:
            return [UIImage imageNamed:@"albumcover_serendipity.jpg"];
            
        default:
            return nil;
    }
}

- (NSIndexPath *)mapMusicType:(kMusicType)musicType {
    switch (musicType) {
        case kMusicTypeChillLounge:
            return [NSIndexPath indexPathForRow:0 inSection:kAlbumTypeFree];
        case kMusicTypeGetaway:
            return [NSIndexPath indexPathForRow:1 inSection:kAlbumTypeFree];
        case kMusicTypeMorningBreeze:
            return [NSIndexPath indexPathForRow:2 inSection:kAlbumTypeFree];
        case kMusicTypeSmoothGrove:
            return [NSIndexPath indexPathForRow:3 inSection:kAlbumTypeFree];
        case kMusicTypeSweethearts:
            return [NSIndexPath indexPathForRow:4 inSection:kAlbumTypeFree];
            
        case kMusicTypeAfter8:
            return [NSIndexPath indexPathForRow:0 inSection:kAlbumTypeMontaggio];
        case kMusicTypeEnchanted:
            return [NSIndexPath indexPathForRow:1 inSection:kAlbumTypeMontaggio];
        case kMusicTypeRipples:
            return [NSIndexPath indexPathForRow:2 inSection:kAlbumTypeMontaggio];
        case kMusicTypeSouthshoreJive:
            return [NSIndexPath indexPathForRow:3 inSection:kAlbumTypeMontaggio];
        case kMusicTypeVega:
            return [NSIndexPath indexPathForRow:4 inSection:kAlbumTypeMontaggio];
            
        case kMusicTypeAfternoonLullaby:
            return [NSIndexPath indexPathForRow:0 inSection:kAlbumTypeParadiso];
        case kMusicTypeHualalai:
            return [NSIndexPath indexPathForRow:1 inSection:kAlbumTypeParadiso];
        case kMusicTypeMojitoSwing:
            return [NSIndexPath indexPathForRow:2 inSection:kAlbumTypeParadiso];
        case kMusicTypeMoonlightStroll:
            return [NSIndexPath indexPathForRow:3 inSection:kAlbumTypeParadiso];
        case kMusicTypeSunsetCabana:
            return [NSIndexPath indexPathForRow:4 inSection:kAlbumTypeParadiso];
            
        case kMusicTypeAngelCity:
            return [NSIndexPath indexPathForRow:0 inSection:kAlbumTypeSerendipity];
        case kMusicTypeEmbrace:
            return [NSIndexPath indexPathForRow:1 inSection:kAlbumTypeSerendipity];
        case kMusicTypeGravity:
            return [NSIndexPath indexPathForRow:2 inSection:kAlbumTypeSerendipity];
        case kMusicTypeSendingMyLove:
            return [NSIndexPath indexPathForRow:3 inSection:kAlbumTypeSerendipity];
        case kMusicTypeShipwrecks:
            return [NSIndexPath indexPathForRow:4 inSection:kAlbumTypeSerendipity];
            
            
        default:
            return [NSIndexPath indexPathForRow:0 inSection:0];
    }
}

- (void)setupAudioPlayBackIfPossible {
    
    NSError *categoryError = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryAmbient error: &categoryError];
    
    if (categoryError) {
        DDLogError(@"%@: %@, Error: %@",THIS_FILE,THIS_METHOD,[categoryError localizedDescription]);
    }
    
    NSError *activationError = nil;
    [[AVAudioSession sharedInstance] setActive:YES error: &activationError];
    
    if (activationError) {
        DDLogError(@"%@: %@, Error: %@",THIS_FILE,THIS_METHOD,[activationError localizedDescription]);
    }
}

#pragma mark - AudioPlayer Delegate

- (void) audioPlayerDidFinishPlaying: (AVAudioPlayer *) player
                        successfully: (BOOL) completed {
    DDLogVerbose(@"%@: %@",THIS_FILE,THIS_FILE);
}

@end
