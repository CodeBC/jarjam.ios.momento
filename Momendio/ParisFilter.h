//
//  ParisFilter.h
//  Momendio
//
//  Created by Nur Iman Izam on 23/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GPUImageFilterGroup.h"

@interface ParisFilter : GPUImageFilterGroup
{
    GPUImagePicture *lookupImageSource;
}

@end
