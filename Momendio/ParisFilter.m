//
//  ParisFilter.m
//  Momendio
//
//  Created by Nur Iman Izam on 23/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ParisFilter.h"

@implementation ParisFilter

- (id)init
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
    UIImage *image = [UIImage imageNamed:@"lookup_paris.png"];
#else
    NSImage *image = [NSImage imageNamed:@"lookup_paris.png"];
#endif
    
    NSAssert(image, @"To use ParisFilter you need to add lookup_paris.png in your application bundle.");
    
    lookupImageSource = [[GPUImagePicture alloc] initWithImage:image];
    GPUImageLookupFilter *lookupFilter = [[GPUImageLookupFilter alloc] init];
    
    [lookupImageSource addTarget:lookupFilter atTextureLocation:1];
    [lookupImageSource processImage];
    
    self.initialFilters = [NSArray arrayWithObjects:lookupFilter, nil];
    self.terminalFilter = lookupFilter;
    
    return self;
}

@end
