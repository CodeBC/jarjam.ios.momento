//
//  Project.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Video;

@interface Project : NSManagedObject

@property (nonatomic, retain) NSNumber * templateType;
@property (nonatomic, retain) NSNumber * orientation;

@property (nonatomic, retain) NSString * musicLocalURLString;
@property (nonatomic, retain) NSNumber * musicType;
@property (nonatomic, retain) NSNumber * musicVolume;

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSString * about;

@property (nonatomic, retain) NSNumber * published;
@property (nonatomic, retain) NSString * publishedLocalURLString;

@property (nonatomic, retain) NSString * textString;
@property (nonatomic, retain) NSNumber * textColorType;
@property (nonatomic, retain) NSNumber * textFontType;
@property (nonatomic, retain) NSNumber * textOffsetX;
@property (nonatomic, retain) NSNumber * textOffsetY;
@property (nonatomic, retain) NSNumber * fontSize;

@property (nonatomic, retain) NSSet *videos;

- (NSString *) getRelativePublishedURL;

@end

@interface Project (CoreDataGeneratedAccessors)

- (void)addVideosObject:(Video *)value;
- (void)removeVideosObject:(Video *)value;
- (void)addVideos:(NSSet *)values;
- (void)removeVideos:(NSSet *)values;

@end
