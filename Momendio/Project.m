//
//  Project.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "Project.h"
#import "Video.h"


@implementation Project

@dynamic templateType;
@dynamic orientation;
@dynamic musicLocalURLString;
@dynamic musicType;
@dynamic musicVolume;
@dynamic name;
@dynamic about;
@dynamic published;
@dynamic publishedLocalURLString;
@dynamic author;
@dynamic videos;
@dynamic textString;
@dynamic textColorType;
@dynamic textFontType;
@dynamic textOffsetX;
@dynamic textOffsetY;
@dynamic fontSize;

- (NSString *) getRelativePublishedURL
{
    // Split current published local url
    NSString *filePath = [[self.publishedLocalURLString componentsSeparatedByString:@"/Documents/"] lastObject];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    NSString *relativePath = [NSString stringWithFormat:@"file://%@/%@", documentsDirectory, filePath];
    return relativePath;
}

@end
