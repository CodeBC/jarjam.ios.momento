//
//  ProjectHeaderCollectionViewCell.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 10/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "PSTCollectionViewCell.h"

@interface ProjectHeaderCollectionViewCell : PSTCollectionReusableView

@property (strong, nonatomic) IBOutlet UILabel *secondsLabel;
@property (strong, nonatomic) IBOutlet UILabel *shotsLabel;

@end
