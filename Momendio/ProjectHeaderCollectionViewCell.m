//
//  ProjectHeaderCollectionViewCell.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 10/9/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ProjectHeaderCollectionViewCell.h"

@implementation ProjectHeaderCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
