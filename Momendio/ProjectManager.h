//
//  ProjectManager.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "Project.h"

typedef enum {
    kProjectTemplate24ClipsBy1Secs,
    kProjectTemplate12ClipsBy2Secs,
    kProjectTemplate8ClipsBy3Secs
} kProjectTemplate;

typedef enum {
    kProjectOrientationPortrait,
    kProjectOrientationLandscape,
    kProjectOrientationSquare
} kProjectOrientation;

typedef enum {
    kVideoCameraSourceFrontCamera,
    kVideoCameraSourceBackCamera
} kVideoCameraSource;

@interface ProjectManager : NSObject

@property (assign, nonatomic) kProjectTemplate projectTemplate;
@property (assign, nonatomic) kProjectOrientation projectOrientation;

+ (ProjectManager *)sharedInstance;
+ (NSString *)getDocsDirectory;
+ (CMTime)CMTimeForProjectTemplate:(kProjectTemplate)projectTemplate;
+ (NSInteger)numberOfClipsForProjectTemplate:(kProjectTemplate)projectTemplate;
+ (NSTimeInterval)numberOfSecondsForProjectTemplate:(kProjectTemplate)projectTemplate;

+ (void)publishProject:(Project *)project withCompletion:(void(^)(NSURL *publishedURL, NSError *error, NSInteger currentCount, BOOL isDone))completion;
+ (void)createCreditsVideoForProject:(Project *)project WithCompletion:(void(^)(NSURL *mergedURL, NSError *error))completion;

+ (void)deleteProject:(Project *)project;

@end
