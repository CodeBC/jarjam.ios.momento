//
//  ProjectManager.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ProjectManager.h"

@implementation ProjectManager

+ (id)sharedInstance
{
    static ProjectManager *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[ProjectManager alloc] init];
    });
    
    return __sharedInstance;
}

+ (void)publishProject:(Project *)project withCompletion:(void(^)(NSURL *filteredURL, NSError *error, NSInteger currentCount, BOOL isDone))completion {
    
    [self filterVideosInProject:project withCompletion:^(NSArray *filteredURLs, NSError *error, NSInteger currentCount, BOOL isDone) {
        
        if (isDone) {
            DDLogVerbose(@"%@: %@, %@",THIS_FILE,THIS_METHOD,filteredURLs);
            
            [ProjectManager createCreditsVideoForProject:project WithCompletion:^(NSURL *creditsURL, NSError *error) {
                
                if (error) {
                    DDLogError(@"Credits Error: %@",error);
                    completion(nil,error,currentCount,YES);
                    return;
                }
                
                NSMutableArray *videoURLArray = [[NSMutableArray alloc] initWithArray:filteredURLs];
                [videoURLArray addObject:creditsURL];
                [SVProgressHUD showWithStatus:@"Merging Videos..." maskType:SVProgressHUDMaskTypeGradient];
                [self mergeVideosWithURLArray:videoURLArray forProject:project WithCompletion:^(NSURL *mergedURL, NSError *error) {
                    completion(mergedURL,error,currentCount,YES);
                }];
                
            }];
        
        } else {
            completion(nil,nil,currentCount,NO);
        }
    }];
    
}

+ (void)filterVideosInProject:(Project *)project withCompletion:(void(^)(NSArray *filteredURLs, NSError *error, NSInteger currentCount, BOOL isDone))completion {
    
    // Get Array of Videos Arranged
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];
    NSArray *videoArray = [[project.videos sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    
    // Perform Filtering Process in separate queue.
    dispatch_queue_t filterQueue;
    filterQueue = dispatch_queue_create("com.nurimanism.filterQueue", NULL);
    
    dispatch_async(filterQueue, ^{
        
        // Use Semaphore to run each filter process one at a time.
        dispatch_semaphore_t filterSemaphore = dispatch_semaphore_create(1);
        
        __block NSInteger videoIndex = 0;
        __block NSMutableArray *filterUrlArray = [NSMutableArray new];
        
        for (Video *video in videoArray) {
            dispatch_semaphore_wait(filterSemaphore, DISPATCH_TIME_FOREVER);
            [[FilterManager sharedInstance] filterVideoAtURL:[NSURL URLWithString:[video getRelativeLocalURL]] withFilter:video.filterType.intValue andCompletion:^(NSURL *filteredURL, NSError *error) {
                DDLogVerbose(@"%@: %@",THIS_FILE,THIS_METHOD);
                
                if (filteredURL) {
                    [filterUrlArray addObject:filteredURL];
                }
                
                videoIndex++;
                if (videoIndex == videoArray.count) {
                    completion(filterUrlArray,nil,videoIndex,YES);
                } else {
                    completion(nil,nil,videoIndex,NO);
                }
                
                dispatch_semaphore_signal(filterSemaphore);
            }];
        }
        
    });
    
}

+ (void)createCreditsVideoForProject:(Project *)project WithCompletion:(void(^)(NSURL *creditsURL, NSError *error))completion {
    
    
    // Get Array of Videos Arranged
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];
    NSArray *videoArray = [[project.videos sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    Video *firstVideo = videoArray[0];
    
    // Get Asset's Track to obtain size
    AVURLAsset *videoAsset = [AVURLAsset URLAssetWithURL:[NSURL URLWithString:[firstVideo getRelativeLocalURL]]  options:@{AVURLAssetPreferPreciseDurationAndTimingKey:@YES}];
    AVAssetTrack *videoTrack = [videoAsset tracksWithMediaType:AVMediaTypeVideo][0];
    
    NSError *error = nil;
    
    // Get Blank Track
    AVURLAsset *blankAsset = [AVURLAsset URLAssetWithURL:[[NSBundle mainBundle] URLForResource:@"blank" withExtension:@"mp4"] options:@{AVURLAssetPreferPreciseDurationAndTimingKey:@YES}];
    AVAssetTrack *blankTrack = [videoAsset tracksWithMediaType:AVMediaTypeVideo][0];
    
    // Create Composition
    AVMutableComposition *creditsComposition = [AVMutableComposition composition];
    
    // Create Composition Track
    AVMutableCompositionTrack *videoCompositionTrack = [creditsComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID: kCMPersistentTrackID_Invalid];
    
    // Insert Video
    [videoCompositionTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, blankAsset.duration) ofTrack:blankTrack atTime:kCMTimeZero error:&error];
    
    // Create AVMutableVideoCompositionInstruction
    AVMutableVideoCompositionInstruction *mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, blankAsset.duration);
    
    //  Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
    AVMutableVideoCompositionLayerInstruction *mainLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:blankTrack];
    [mainLayerInstruction setTransform:videoTrack.preferredTransform atTime:kCMTimeZero];
    
    AVMutableVideoCompositionInstruction *orientationCompositionInst = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    orientationCompositionInst.timeRange = CMTimeRangeMake(kCMTimeZero,blankAsset.duration);
    orientationCompositionInst.layerInstructions = @[mainLayerInstruction];
    
    AVMutableVideoComposition *orientationComposition = [AVMutableVideoComposition videoComposition];
    orientationComposition.frameDuration = CMTimeMake(1, 30);
    orientationComposition.instructions = @[orientationCompositionInst];

    CGFloat actualHeight;
    CGFloat actualWidth;
    
    if ([project.orientation isEqualToNumber:@(kProjectOrientationPortrait)]) {
        actualWidth = 720.0f;
        actualHeight = 1280.0f;
    } else if ([project.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        actualWidth = 720.0f;
        actualHeight = 720.0f;
    } else {
        actualWidth = 1280.0f;
        actualHeight = 720.0f;
    }
    
    // Set up the text layer
    NSString *titleText = [NSString stringWithFormat:@"%@\n\n",project.name];
    CATextLayer *titleLayer = [[CATextLayer alloc] init];
    titleLayer.truncationMode = kCATruncationEnd;
    titleLayer.wrapped = YES;
    titleLayer.string = titleText;
    titleLayer.alignmentMode = kCAAlignmentCenter;
    titleLayer.foregroundColor = [[UIColor whiteColor] CGColor];
    [titleLayer setFont:@"AvenirNext-Medium"];
    [titleLayer setFontSize:40.0f];
    
    if ([project.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [project.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        [titleLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    } else {
        [titleLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    }
    
    UIGraphicsBeginImageContext([titleLayer frame].size);
    [titleLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *titleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CALayer *titleImageLayer = [CALayer layer];
    [titleImageLayer setContents:(id)[titleImage CGImage]];
    
    if ([project.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [project.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        [titleImageLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    } else {
        [titleImageLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    }
    
    [titleImageLayer setMasksToBounds:YES];
    
    // Set up Created By layer
    NSString *createdByText = [NSString stringWithFormat:@"\n\n\n\nCreated by"];
    CATextLayer *createdByTextLayer = [[CATextLayer alloc] init];
    createdByTextLayer.truncationMode = kCATruncationEnd;
    createdByTextLayer.wrapped = YES;
    createdByTextLayer.string = createdByText;
    createdByTextLayer.alignmentMode = kCAAlignmentCenter;
    createdByTextLayer.foregroundColor = [[UIColor whiteColor] CGColor];
    [createdByTextLayer setFont:@"AvenirNext-Italic"];
    [createdByTextLayer setFontSize:28.0f];
    
    if ([project.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [project.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        [createdByTextLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    } else {
        [createdByTextLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    }
    
    UIGraphicsBeginImageContext([createdByTextLayer frame].size);
    [createdByTextLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *createdByImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CALayer *createdByImageLayer = [CALayer layer];
    [createdByImageLayer setContents:(id)[titleImage CGImage]];
    
    if ([project.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [project.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        [createdByImageLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    } else {
        [createdByImageLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    }
    
    [createdByImageLayer setMasksToBounds:YES];
    
    // Set up Created By layer
    NSString *nameText = [NSString stringWithFormat:@"\n\n\n\n%@", project.author];
    CATextLayer *nameTextLayer = [[CATextLayer alloc] init];
    nameTextLayer.truncationMode = kCATruncationEnd;
    nameTextLayer.wrapped = YES;
    nameTextLayer.string = nameText;
    nameTextLayer.alignmentMode = kCAAlignmentCenter;
    nameTextLayer.foregroundColor = [[UIColor whiteColor] CGColor];
    [nameTextLayer setFont:@"AvenirNext-MediumItalic"];
    [nameTextLayer setFontSize:36.0f];
    
    if ([project.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [project.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        [nameTextLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    } else {
        [nameTextLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    }
    
    UIGraphicsBeginImageContext([createdByTextLayer frame].size);
    [nameTextLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *nameTextImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CALayer *nameTextImageLayer = [CALayer layer];
    [nameTextImageLayer setContents:(id)[titleImage CGImage]];
    
    if ([project.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [project.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
        [nameTextImageLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    } else {
        [nameTextImageLayer setFrame:CGRectMake(10.0f, 0.0f, actualWidth-20.0f, actualHeight/2 + 40.0f)];
    }
    
    [nameTextImageLayer setMasksToBounds:YES];

    
    // Set up logo layer
    CALayer *logoLayer = [CALayer layer];
    UIImage *logoImage = [UIImage imageNamed:@"logo"];
    [logoLayer setContents:(id)[logoImage CGImage]];
    logoLayer.frame = CGRectMake(actualWidth-30.0f-242.0f, 30.0f, 242.0f, 80.0f);
    [logoLayer setMasksToBounds:YES];
    
    // The main overlay
    CALayer *overlayLayer = [CALayer layer];
    overlayLayer.frame = CGRectMake(0, 0, actualWidth, actualHeight);
    overlayLayer.backgroundColor = [[UIColor blackColor] CGColor];
    [overlayLayer addSublayer:logoLayer];
    [overlayLayer addSublayer:titleLayer];
    [overlayLayer addSublayer:createdByTextLayer];
    [overlayLayer addSublayer:nameTextLayer];
    [overlayLayer setMasksToBounds:YES];
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, actualWidth, actualHeight);
    videoLayer.frame = CGRectMake(0, 0, actualWidth, actualHeight);
    [parentLayer addSublayer:videoLayer]; 
    [parentLayer addSublayer:overlayLayer];
    [parentLayer addSublayer:logoLayer];
    [parentLayer addSublayer:titleImageLayer];
    [parentLayer addSublayer:createdByImageLayer];
    [parentLayer addSublayer:nameTextImageLayer];
    NSLog(@"Title text image %f - %f", titleImageLayer.frame.origin.x, titleImageLayer.frame.origin.y);
    NSLog(@"Created text image %f - %f", createdByImageLayer.frame.origin.x, createdByImageLayer.frame.origin.y);
    NSLog(@"Name text image %f - %f", nameTextImageLayer.frame.origin.x, nameTextImageLayer.frame.origin.y);
    
    orientationComposition.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
    orientationComposition.renderSize = CGSizeMake(actualWidth, actualHeight);
    
    AVAssetExportSession *assetExport = [[AVAssetExportSession alloc] initWithAsset:creditsComposition presetName:AVAssetExportPresetHighestQuality];
    assetExport.videoComposition = orientationComposition;
    
    NSString *mergedFileURLString = [[self getDocsDirectory] stringByAppendingFormat:@"/credits.mov"];
    NSURL *mergedFileURL = [NSURL fileURLWithPath:mergedFileURLString];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:mergedFileURLString]) {
        [[NSFileManager defaultManager] removeItemAtPath:mergedFileURLString error:nil];
    }
    
    assetExport.outputFileType = AVFileTypeQuickTimeMovie;
    assetExport.outputURL = mergedFileURL;
    assetExport.shouldOptimizeForNetworkUse = YES;
    
    [assetExport exportAsynchronouslyWithCompletionHandler:^(void ) {
        
        DDLogVerbose(@"%@: %@, Export Status: %d",THIS_FILE,THIS_METHOD,assetExport.status);
        
        switch (assetExport.status) {
            case AVAssetExportSessionStatusCompleted:
                if (completion) completion(mergedFileURL,nil);
                break;
                
            case AVAssetExportSessionStatusFailed:
                if (completion) completion(nil,assetExport.error);
                break;
                
            case AVAssetExportSessionStatusCancelled:
                if (completion) completion(nil,assetExport.error);
                break;
                
            default:
                if (completion) completion(nil,[NSError errorWithDomain:@"Export Error" code:AVAssetExportSessionStatusFailed userInfo:@{ NSLocalizedDescriptionKey : @"Unknown Export Error" }]);
                break;
        }
     }];

}

+ (void)mergeVideosWithURLArray:(NSArray*)urlArray forProject:(Project *)project WithCompletion:(void(^)(NSURL *mergedURL, NSError *error))completion {
    
    NSError *error = nil;
    
    // Create Composition
    AVMutableComposition *mergeComposition = [AVMutableComposition composition];
    
    // Create Video Track
    AVMutableCompositionTrack *videoCompositionTrack = [mergeComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];

    // Create Layer Instructions Array
    NSMutableArray *videoLayerInstructions = [NSMutableArray new];
    
    // Use Last Track as Reference
    AVURLAsset *lastAsset = [AVURLAsset URLAssetWithURL:[urlArray lastObject] options:@{AVURLAssetPreferPreciseDurationAndTimingKey:@YES}];
    AVAssetTrack *lastTrack = [lastAsset tracksWithMediaType:AVMediaTypeVideo][0];
    
    // Create VideoComposition to apply transforms
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    // Set Video Size
    CGSize renderSize = CGSizeApplyAffineTransform(lastTrack.naturalSize, lastTrack.preferredTransform);
    if (renderSize.height < 0) renderSize.height = -renderSize.height;
    if (renderSize.width < 0) renderSize.width = -renderSize.width;
    DDLogVerbose(@"Render Size: %f %f. Natural Size: %f %f.",renderSize.width,renderSize.height,lastTrack.naturalSize.width,lastTrack.naturalSize.height);
    
    videoComposition.renderSize = renderSize;
    
    // Display Text Effect if exists.
    if (project.textString) {
    
        CGFloat actualWidth = renderSize.width;
        CGFloat actualHeight = renderSize.height;
        
        // Set up the text layer
        NSString *titleText = [NSString stringWithFormat:@"%@",project.textString];
        CATextLayer *titleLayer = [[CATextLayer alloc] init];
        titleLayer.truncationMode = kCATruncationEnd;
        titleLayer.wrapped = YES;
        titleLayer.string = titleText;
        titleLayer.alignmentMode = kCAAlignmentCenter;
        titleLayer.foregroundColor = [[Constants colorForColorType:project.textColorType.intValue] CGColor];
        [titleLayer setFont:(__bridge CFTypeRef)([Constants fontForFontType:project.textFontType.intValue atSize:10.0f].fontName)];
        [titleLayer setFontSize:project.fontSize.floatValue*2];
        [titleLayer setFrame:CGRectMake(project.textOffsetX.floatValue*2, -project.textOffsetY.floatValue*2, actualWidth, actualHeight/2 + 40.0f)];
        
        UIGraphicsBeginImageContext([titleLayer frame].size);
        [titleLayer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *titleImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CALayer *titleImageLayer = [CALayer layer];
        [titleImageLayer setContents:(id)[titleImage CGImage]];
        [titleImageLayer setFrame:CGRectMake(project.textOffsetX.floatValue*2, -project.textOffsetY.floatValue*2, actualWidth, actualHeight/2 + 40.0f)];
        [titleImageLayer setMasksToBounds:YES];
        
        // The main overlay
        CALayer *overlayLayer = [CALayer layer];
        overlayLayer.frame = CGRectMake(0, 0, actualWidth, actualHeight);
        overlayLayer.backgroundColor = [[UIColor clearColor] CGColor];
        [overlayLayer addSublayer:titleLayer];
        [overlayLayer setMasksToBounds:YES];
        
        CALayer *parentLayer = [CALayer layer];
        CALayer *videoLayer = [CALayer layer];
        parentLayer.frame = CGRectMake(0, 0, actualWidth, actualHeight);
        videoLayer.frame = CGRectMake(0, 0, actualWidth, actualHeight);
        [parentLayer addSublayer:videoLayer];
        [parentLayer addSublayer:overlayLayer];
        
        // Fade Animation
        CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeAnimation.duration = 1.0f;
        fadeAnimation.fromValue = @1.0f;
        fadeAnimation.toValue = @0.0f;
        fadeAnimation.removedOnCompletion = NO;
        fadeAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CMTimeGetSeconds(CMTimeMakeWithSeconds(23.0f, 30.0f));
        [overlayLayer addAnimation:fadeAnimation forKey:nil];
        
        // Continue Hiding Animation
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation.duration = 4.0f;
        animation.fromValue = @0.0f;
        animation.toValue = @0.0f;
        animation.removedOnCompletion = NO;
        animation.beginTime = AVCoreAnimationBeginTimeAtZero + CMTimeGetSeconds(CMTimeMakeWithSeconds(24.0f, 30.0f));
        [overlayLayer addAnimation:animation forKey:nil];
        
        videoComposition.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
        
    } // End Text Effect
    
    // Add All Videos to Composition
    for (NSURL *videoUrl in urlArray) {
        
        DDLogVerbose(@"Inserting %@",videoUrl);
        
        // Check if clip exists
        if (![[NSFileManager defaultManager] fileExistsAtPath:[videoUrl path]]) {
            DDLogVerbose(@"Clip not found at %@",videoUrl);
            continue;
        }
        
        // Load Clip
        AVURLAsset *videoAsset = [AVURLAsset URLAssetWithURL:videoUrl options:@{AVURLAssetPreferPreciseDurationAndTimingKey:@YES}];
        
        NSError *outError = nil;
        AVAssetReader *assetReader = [AVAssetReader assetReaderWithAsset:videoAsset error:&outError];
        if (assetReader == nil) {
            DDLogVerbose(@"Asset Reader for %@ Error: %@",videoUrl,[outError localizedDescription]);
            continue;
        }
        
        AVAssetTrack *videoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
        
        if (!videoTrack) { // Ensure there's a video track
            DDLogVerbose(@"%@: %@, Video Track Missing for %@",THIS_FILE,THIS_METHOD,videoUrl.absoluteString);
            continue;
        }
        
        DDLogVerbose(@"Track Natural Size %f %f",videoTrack.naturalSize.width,videoTrack.naturalSize.height);
        
        // Obtain clip timings
        CMTime projectClipStart = [mergeComposition duration];
        CMTime projectClipDuration;
        
        if ([videoUrl isEqual:[urlArray lastObject]]) {
            projectClipDuration = CMTimeSubtract(CMTimeMakeWithSeconds(27.0f, 30.0f), projectClipStart);
        } else {
            projectClipDuration = [videoAsset duration];
        }
        
        CMTime projectClipEnd = CMTimeAdd(projectClipStart, projectClipDuration);
        
        // Insert VideoTrack into VideoTrackComposition
        [videoCompositionTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero,projectClipDuration)  ofTrack:videoTrack atTime:[mergeComposition duration] error:&error];
        
        if (error && completion) {
            completion(nil,error);
            return;
        }
        
        // Create Video Layer Instruction
        AVMutableVideoCompositionLayerInstruction *videoLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
        
        // Inspect Transform
        UIImageOrientation firstAssetOrientation_  = UIImageOrientationUp;
        BOOL isFirstAssetPortrait_  = NO;
        CGAffineTransform firstTransform = videoTrack.preferredTransform;
        if (firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0) {
            firstAssetOrientation_ = UIImageOrientationRight;
            isFirstAssetPortrait_ = YES;
        }
        if (firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0) {
            firstAssetOrientation_ =  UIImageOrientationLeft;
            isFirstAssetPortrait_ = YES;
        }
        if (firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0) {
            firstAssetOrientation_ =  UIImageOrientationUp;
        }
        if (firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {
            firstAssetOrientation_ = UIImageOrientationDown;
        }
        
        if (firstAssetOrientation_ == UIImageOrientationUp) {
            DDLogVerbose(@"Up");
        } else if (firstAssetOrientation_ == UIImageOrientationDown) {
            DDLogVerbose(@"Down");
        } else if (firstAssetOrientation_ == UIImageOrientationRight) {
            DDLogVerbose(@"Right");
        } else if (firstAssetOrientation_ == UIImageOrientationLeft) {
            DDLogVerbose(@"Left");
        }
        
        // Set Transforms for Video

        if (firstAssetOrientation_ == UIImageOrientationLeft) {
//            [videoLayerInstruction setTransformRampFromStartTransform:CGAffineTransformIdentity toEndTransform:CGAffineTransformConcat(videoTrack.preferredTransform, CGAffineTransformMakeTranslation(0, renderSize.height)) timeRange:CMTimeRangeMake(projectClipStart, projectClipDuration)];
            [videoLayerInstruction setTransform:CGAffineTransformConcat(videoTrack.preferredTransform, CGAffineTransformMakeTranslation(0, renderSize.height)) atTime:projectClipStart];
        } else if(firstAssetOrientation_ == UIImageOrientationRight) {
//            [videoLayerInstruction setTransformRampFromStartTransform:CGAffineTransformIdentity toEndTransform:CGAffineTransformConcat(videoTrack.preferredTransform, CGAffineTransformMakeTranslation(renderSize.width, 0)) timeRange:CMTimeRangeMake(projectClipStart, projectClipDuration)];
            [videoLayerInstruction setTransform:CGAffineTransformConcat(videoTrack.preferredTransform, CGAffineTransformMakeTranslation(renderSize.width, 0)) atTime:projectClipStart];
        } else {
//            [videoLayerInstruction setTransformRampFromStartTransform:CGAffineTransformIdentity toEndTransform:videoTrack.preferredTransform timeRange:CMTimeRangeMake(projectClipStart, projectClipDuration)];
            [videoLayerInstruction setTransform:videoTrack.preferredTransform atTime:projectClipStart];
        }
        
        [videoLayerInstruction setOpacity:0.0 atTime:projectClipEnd];
        [videoLayerInstructions addObject:videoLayerInstruction];
    }

    // Add Music Track if available
    if (project.musicLocalURLString) {
        NSURL *musicUrl = [NSURL fileURLWithPath:project.musicLocalURLString];
        
        // Add Music From Start to End of Composition
        AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:musicUrl options:@{AVURLAssetPreferPreciseDurationAndTimingKey:@YES}];
        AVAssetTrack *audioAssetTrack = [audioAsset tracksWithMediaType:AVMediaTypeAudio][0];
        
        // Insert Audio Track
        AVMutableCompositionTrack *musicCompositionTrack = [mergeComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        
        // Insert Audio Segment into Audio Track
        [musicCompositionTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [mergeComposition duration])  ofTrack:audioAssetTrack atTime:kCMTimeZero error:&error];
        
        if (error && completion) {
            DDLogError(@"%@: %@, Music Error: %@",THIS_FILE,THIS_METHOD,[error localizedDescription]);
            completion(nil,error);
            return;
        }
    }

    // Output Path
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MM-yyyy-HH-mm-ss"];
    
    NSString *mergedFileURLString = [[self getDocsDirectory] stringByAppendingFormat:@"/%@-merged.mov",[dateFormatter stringFromDate:[NSDate date]]];
    NSURL *mergedFileURL = [NSURL fileURLWithPath:mergedFileURLString];
    
//    GPUImageMovie *movie = [[GPUImageMovie alloc] initWithAsset:mergeComposition];
//    GPUImageMovieWriter *writer = [[GPUImageMovieWriter alloc] initWithMovieURL:mergedFileURL size:videoComposition.renderSize];
//    writer.shouldPassthroughAudio = YES;
//    movie.audioEncodingTarget = writer;
//    [movie enableSynchronizedEncodingUsingMovieWriter:writer];
//    
//    __unsafe_unretained GPUImageMovieWriter *weakWriter = writer;
//    writer.completionBlock = ^{
//        DDLogVerbose(@"Completion Handler Called");
//        [movie removeTarget:weakWriter];
//        [weakWriter finishRecording];
//        
//        if (completion) completion(mergedFileURL,nil);
//    };
//    
//    [movie addTarget:writer];
//    [movie startProcessing];
//    [writer startRecording];
    
    // Setup Export Video
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:mergeComposition presetName:AVAssetExportPresetHighestQuality];
    exportSession.outputURL = mergedFileURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.shouldOptimizeForNetworkUse = YES;
    exportSession.timeRange = CMTimeRangeMake(kCMTimeZero, [mergeComposition duration]);
    
    // Setup Main Composition Instructions
    AVMutableVideoCompositionInstruction *videoCompositionInst = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    videoCompositionInst.timeRange = CMTimeRangeMake(kCMTimeZero,[mergeComposition duration]);
    videoCompositionInst.layerInstructions = videoLayerInstructions;
    videoComposition.instructions = @[videoCompositionInst];
    
    // Export Video    
    exportSession.videoComposition = videoComposition;
    
    DDLogVerbose(@"Exporting Video: %@",mergedFileURL);
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        DDLogVerbose(@"%@: %@, Export Status: %d",THIS_FILE,THIS_METHOD,exportSession.status);
        
        switch (exportSession.status) {
            case AVAssetExportSessionStatusCompleted:
                if (completion) completion(mergedFileURL,nil);
                break;
                
            case AVAssetExportSessionStatusFailed:
                if (completion) completion(nil,exportSession.error);
                break;
                
            case AVAssetExportSessionStatusCancelled:
                if (completion) completion(nil,exportSession.error);
                break;
                
            default:
                if (completion) completion(nil,[NSError errorWithDomain:@"Export Error" code:AVAssetExportSessionStatusFailed userInfo:@{ NSLocalizedDescriptionKey : @"Unknown Export Error" }]);
                break;
        }
        
        [self deleteFilteredVideosForURLArray:urlArray];
        
    }];
    
}

+ (void)deleteFilteredVideosForURLArray: (NSArray*)urlArray {
    
    for (NSURL *videoURL in urlArray) {
    
        // Generate Filtered URL
        NSString *oldFileNameWithoutExtension = [[[videoURL absoluteString] lastPathComponent] stringByDeletingPathExtension];
        NSString *newFileName = [NSString stringWithFormat:@"%@-filtered.mov",oldFileNameWithoutExtension];
        
        NSString *pathToMovie = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",newFileName];
        DDLogVerbose(@"%@:%@, %@",THIS_FILE,THIS_METHOD,pathToMovie);
        unlink([pathToMovie UTF8String]);
    }
    
}

+ (NSString *)getDocsDirectory {
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSLog(@"Storing ind document %@", documentsDirectory);
    
    return documentsDirectory;
}

+ (CMTime)CMTimeForProjectTemplate:(kProjectTemplate)projectTemplate {
    switch (projectTemplate) {
        case kProjectTemplate24ClipsBy1Secs:
            return CMTimeMake(35, 30);
            break;
            
        case kProjectTemplate12ClipsBy2Secs:
            return CMTimeMake(65, 30);
            break;
            
        case kProjectTemplate8ClipsBy3Secs:
            return CMTimeMake(95, 30);
            break;
    }
}

+ (NSInteger)numberOfClipsForProjectTemplate:(kProjectTemplate)projectTemplate {
    switch (projectTemplate) {
        case kProjectTemplate24ClipsBy1Secs:
            return 24;
            break;
            
        case kProjectTemplate12ClipsBy2Secs:
            return 12;
            break;
            
        case kProjectTemplate8ClipsBy3Secs:
            return 8;
            break;
    }
}

+ (NSTimeInterval)numberOfSecondsForProjectTemplate:(kProjectTemplate)projectTemplate {
    switch (projectTemplate) {
        case kProjectTemplate24ClipsBy1Secs:
            return 1.0f;
            break;
            
        case kProjectTemplate12ClipsBy2Secs:
            return 2.0f;
            break;
            
        case kProjectTemplate8ClipsBy3Secs:
            return 3.0f;
            break;
    }
}

+ (void)deleteProject:(Project *)project {
    NSArray *videoArray = [project.videos allObjects];
    
    for (Video *video in videoArray) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:[video getRelativeLocalURL]] error:&error];
        
        if (error) {
            DDLogError(@"%@",[error localizedDescription]);
        }
        
        [video deleteEntity];
    }
    
    if (project.published) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:[project getRelativePublishedURL]] error:&error];
        
        if (error) {
            DDLogError(@"%@",[error localizedDescription]);
        }
    }
    
    [project deleteEntity];
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
}

@end
