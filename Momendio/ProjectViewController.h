//
//  ProjectViewController.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ProjectViewController : UIViewController

@property Project *currentProject;

@end
