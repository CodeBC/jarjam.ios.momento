//
//  ProjectViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ProjectViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "PSTCollectionView.h"
#import "LXReorderableCollectionViewFlowLayout.h"

#import "CaptureViewController.h"
#import "FilterTextViewController.h"

#import "VideoCollectionViewCell.h"
#import "ProjectHeaderCollectionViewCell.h"

#import "AppDelegate.h"

@interface ProjectViewController () <CaptureViewControllerDelegate, LXReorderableCollectionViewDataSource, LXReorderableCollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSMutableArray *videoArray;

@property (strong, nonatomic) MPMoviePlayerController *videoPlayer;
@property (strong, nonatomic) AVPlayerItem *playerItem;

@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (strong, nonatomic) IBOutlet PSTCollectionView *collectionView;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)galleryButtonPressed:(id)sender;
- (IBAction)settingsButtonPressed:(id)sender;
- (IBAction)editButtonPressed:(id)sender;

@end

@implementation ProjectViewController

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];
    [self setupNewProjectIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateVideoArray];
    [self setupVideoPlayBackIfPossible];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate showProjectOverlayIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.videoPlayer) {
        [self.videoPlayer pause];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PSTCollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView: (PSTCollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(PSTCollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [ProjectManager numberOfClipsForProjectTemplate:self.currentProject.templateType.intValue];
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    VideoCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"VideoCollectionViewCell" forIndexPath:indexPath];
    [cell setStyling];
    
    if (indexPath.row >= self.videoArray.count) { // Empty Spot
        
        
        cell.layer.borderColor = [[UIColor colorWithRed:79.0f/255.0f green:79.0f/255.0f blue:79.0f/255.0f alpha:1.0f] CGColor];
        cell.layer.borderWidth = 1.0f;
        
        cell.thumbnailImageView.hidden = YES;
        cell.playImageView.hidden = YES;
        cell.clipNumberLabel.text = [NSString stringWithFormat:@"%d",indexPath.row+1];
        
    } else { // Contains a video
    
        Video *cellVideo = self.videoArray[indexPath.row];
        
        cell.thumbnailImageView.image = [UIImage imageWithData:cellVideo.thumbnailData];
        if (!cell.thumbnailImageView.image) {
            [cellVideo refreshThumbnail];
            cell.thumbnailImageView.image = [UIImage imageWithData:cellVideo.thumbnailData];
        }
        
        cell.playImageView.hidden = NO;
        cell.thumbnailImageView.hidden = NO;
        
        if (indexPath.row == self.selectedIndexPath.row) {
            cell.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
            cell.layer.borderWidth = 2.0f;
        } else {
            cell.layer.borderColor = [[UIColor colorWithRed:79.0f/255.0f green:79.0f/255.0f blue:79.0f/255.0f alpha:1.0f] CGColor];
            cell.layer.borderWidth = 1.0f;
        }
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            if (state == UIGestureRecognizerStateRecognized) {
            
                [self.videoPlayer stop];
                [self.videoPlayer setContentURL:[NSURL URLWithString:[cellVideo getRelativeLocalURL]]];
                [self.videoPlayer play];
                
                self.selectedIndexPath = indexPath;
                
                [NSTimer bk_scheduledTimerWithTimeInterval:0.2f block:^(NSTimer *timer) {
                    [self.collectionView reloadData];
                } repeats:NO];
                
            }
            
        }];
        
        [cell addGestureRecognizer:tapGesture];
        
        UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            if (state == UIGestureRecognizerStateRecognized) {
                
                UIAlertView *alertView = [UIAlertView bk_alertViewWithTitle:@"Delete Selected Video" message:@"Are your sure?"];
                [alertView bk_addButtonWithTitle:@"Cancel" handler:^{
                    cell.layer.borderWidth = 0.0f;
                }];
                [alertView bk_addButtonWithTitle:@"Delete" handler:^{
                    
                    [SVProgressHUD showWithStatus:@"Deleting..." maskType:SVProgressHUDMaskTypeGradient];
                    Video *selectedVideo = self.videoArray[indexPath.row];
                    [self.currentProject removeVideosObject:selectedVideo];
                    
                    NSError *error = nil;
                    BOOL removeFileSuccess = [[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:[selectedVideo getRelativeLocalURL]] error:&error];
                    
                    if (!removeFileSuccess) {
                        DDLogError(@"%@: %@, Remove File Error: %@",THIS_FILE,THIS_METHOD,[error localizedDescription]);
                    }
                    
                    [selectedVideo deleteEntity];
                    [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
                    [self updateVideoArray];
                    [self.videoPlayer setContentURL:[NSURL URLWithString:[cellVideo getRelativeLocalURL]]];
                    [SVProgressHUD dismiss];
                    cell.layer.borderWidth = 0.0f;
                    
                }];
                [alertView show];
            }
            
        }];
        
        doubleTapGesture.numberOfTapsRequired = 2;        
        [cell addGestureRecognizer:doubleTapGesture];
    }
    
    return cell;
}

- (PSTCollectionReusableView *)collectionView: (PSTCollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
        
    ProjectHeaderCollectionViewCell *supplementaryView = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"HeaderCell" forIndexPath:indexPath];
    
    supplementaryView.secondsLabel.text = [NSString stringWithFormat:@"%.0f",[ProjectManager numberOfSecondsForProjectTemplate:self.currentProject.templateType.intValue]];
    supplementaryView.shotsLabel.text = [NSString stringWithFormat:@"%d",[ProjectManager numberOfClipsForProjectTemplate:self.currentProject.templateType.intValue]];
    
    return  supplementaryView;
};

- (BOOL)collectionView:(PSTCollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath {
    
    if (toIndexPath.item >= self.videoArray.count) {
        return NO;
    } else {
        return YES;
    }
}

- (void)collectionView:(PSTCollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath willMoveToIndexPath:(NSIndexPath *)toIndexPath {
    
    Video *fromVideo = self.videoArray[fromIndexPath.row];
    
    [self.videoArray removeObjectAtIndex:fromIndexPath.item];
    [self.videoArray insertObject:fromVideo atIndex:toIndexPath.item];
    [self rebuildSortOrder];
}

- (void)rebuildSortOrder {
    for (NSInteger i = 0; i < self.videoArray.count; i++) {
        Video *video = self.videoArray[i];
        video.sortOrder = @(i);
    }
}

#pragma mark – PSTCollectionViewDelegateFlowLayout

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(70, 70);
}

- (UIEdgeInsets)collectionView:
(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 20, 10, 20);
}

- (CGFloat)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(320, 60);
}


#pragma mark - Button Actions

- (IBAction)cameraButtonPressed:(id)sender {
    DDLogVerbose(@"Camera Button Pressed");
    if (self.currentProject.videos.count == [ProjectManager numberOfClipsForProjectTemplate:[self.currentProject.templateType intValue]]) {
        [SVProgressHUD showErrorWithStatus:@"You've taken maximum number of videos. Try deleting those not in use"];
    } else {
        if ([self.currentProject.orientation isEqualToNumber:@(kProjectOrientationPortrait)] || [self.currentProject.orientation isEqualToNumber:@(kProjectOrientationSquare)]) {
            [self performSegueWithIdentifier:@"ProjectToCapture" sender:self];
        } else {
            [self performSegueWithIdentifier:@"ProjectToCaptureLandscape" sender:self];
        }
    }
}

- (IBAction)galleryButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ProjectToGallery" sender:self];
}

- (IBAction)settingsButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ProjectToSettings" sender:self];
}

- (IBAction)editButtonPressed:(id)sender {
    if (self.currentProject.videos.count == [ProjectManager numberOfClipsForProjectTemplate:[self.currentProject.templateType intValue]]) {
        [self performSegueWithIdentifier:@"ProjectToFilterText" sender:self];
    } else {
        NSString *errorMessage = [NSString stringWithFormat:@"Capture all %d clips before proceeding",[ProjectManager numberOfClipsForProjectTemplate:[self.currentProject.templateType intValue]]];
        [SVProgressHUD showErrorWithStatus:errorMessage];
    }
}

- (void)backButtonPressed {
    BOOL projectDeleted = [self deleteProjectIfEmpty];
    if(!projectDeleted) {
        [self promptForNameIfNotExist];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - Helper Functions

- (void)setStyling {
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"New" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [backButton setTitleTextAttributes:
     @{ UITextAttributeFont: [UIFont fontWithName:@"Avenir-Medium" size:17.0f],
        UITextAttributeTextColor: [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.editButton setBackgroundImage:[[UIImage alloc]init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1];
    [self.collectionView registerNib:[UINib nibWithNibName:@"VideoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"VideoCollectionViewCell"];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ProjectHeaderCollectionViewCell" bundle:nil] forSupplementaryViewOfKind:@"UICollectionElementKindSectionHeader" withReuseIdentifier:@"HeaderCell"];
    
    [self.collectionView setCollectionViewLayout:[LXReorderableCollectionViewFlowLayout new] animated:YES];
}

- (void)setupNewProjectIfNeeded {
    if (!self.currentProject) {
        self.currentProject = [Project createEntity];
        self.currentProject.templateType = @([[ProjectManager sharedInstance] projectTemplate]);
        self.currentProject.orientation = @([[ProjectManager sharedInstance] projectOrientation]);
    }
}

- (void)updateVideoArray {
    if (self.currentProject && self.currentProject.videos.count) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];
        self.videoArray = [[self.currentProject.videos sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    } else {
        self.videoArray = [@[] mutableCopy];
    }
    
    [self.collectionView reloadData];
}

- (void)setupVideoPlayBackIfPossible {
    if (self.videoArray && self.videoArray.count) {
        Video *aVideo = self.videoArray[0];
        self.videoPlayer = [[MPMoviePlayerController alloc] initWithContentURL: [NSURL URLWithString:[aVideo getRelativeLocalURL]]];
        self.videoPlayer.allowsAirPlay = NO;
        self.videoPlayer.shouldAutoplay = NO;
        [self.videoPlayer prepareToPlay];
        [self.videoPlayer.view setFrame: self.videoView.bounds];  // player's frame must match parent's
        [self.videoView addSubview: self.videoPlayer.view];
    } else {
        // Remove if already attached
        if (self.videoPlayer) {
            [self.videoPlayer.view removeFromSuperview];
            self.videoPlayer = nil;
        }
    }
}

- (BOOL)deleteProjectIfEmpty {
    if (self.currentProject && !self.currentProject.videos.count) {
        [self.currentProject deleteEntity];
        [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
        return YES;
    }
    
    return NO;
}

- (void)promptForNameIfNotExist {
    
    if (self.currentProject.name) { // Skip if already exists
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] bk_initWithTitle:@"Project Name Required" message:@"Please enter Project Name"];
    [alert bk_addButtonWithTitle:@"Cancel" handler:nil];
    
    __weak UIAlertView *weakAlert = alert;
    [alert bk_addButtonWithTitle:@"Save" handler:^{
        UITextField *nameTextField = [weakAlert textFieldAtIndex:0];
        DDLogVerbose(@"Project Name: %@",nameTextField.text);
        self.currentProject.name = nameTextField.text;
        [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

#pragma mark - Prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DDLogVerbose(@"Prepare for Segue");
    if ([segue.identifier isEqualToString:@"ProjectToCapture"] || [segue.identifier isEqualToString:@"ProjectToCaptureLandscape"]) {
        CaptureViewController *captureView = segue.destinationViewController;
        captureView.delegate = self;
        captureView.currentProject = self.currentProject;
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    } else if ([segue.identifier isEqualToString:@"ProjectToFilterText"]) {
        FilterTextViewController *filterView = segue.destinationViewController;
        filterView.currentProject = self.currentProject;
    }
}

#pragma mark - CaptureViewDelegate

- (void)captureView:(id)captureView didCaptureVideo:(Video *)video {
    
    video.project = self.currentProject;
    video.sortOrder = @(self.currentProject.videos.count);
    
    [self.currentProject addVideosObject:video];
    
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
}

@end
