//
//  RainbowFilter.h
//  Momendio
//
//  Created by Nur Iman Izam on 23/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GPUImageFilterGroup.h"

@interface RainbowFilter : GPUImageFilterGroup
{
    GPUImageHardLightBlendFilter *hardLightBlendFilter;
    GPUImageOverlayBlendFilter *overlayBlendFilter;
    GPUImagePicture *rainbowImageSource;
    GPUImagePicture *inputImageSource;
}

@end
