//
//  RainbowFilter.m
//  Momendio
//
//  Created by Nur Iman Izam on 23/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "RainbowFilter.h"

@implementation RainbowFilter

- (id)init
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
    UIImage *rainbowImage = [UIImage imageNamed:@"rainbow_layer.png"];
#else
    NSImage *rainbowImage = [NSImage imageNamed:@"rainbow_layer.png"];
#endif
    
    NSAssert(rainbowImage, @"To use RainbowFilter you need to add rainbow_layer.png to your application bundle.");
    
    hardLightBlendFilter = [[GPUImageHardLightBlendFilter alloc] init];
    [self addFilter:hardLightBlendFilter];
    
    overlayBlendFilter = [[GPUImageOverlayBlendFilter alloc] init];
    [self addFilter:overlayBlendFilter];
    
    rainbowImageSource = [[GPUImagePicture alloc] initWithImage:rainbowImage];
    
    [rainbowImageSource processImage];
    [rainbowImageSource addTarget:overlayBlendFilter atTextureLocation:1];
    
    self.initialFilters = @[hardLightBlendFilter,overlayBlendFilter];
    self.terminalFilter = overlayBlendFilter;
    
    return self;
}

@end
