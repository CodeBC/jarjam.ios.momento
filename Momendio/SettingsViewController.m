//
//  SettingsViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "SettingsViewController.h"

#import "iRate.h"

#import "YouTubeController.h"
#import "AppDelegate.h"

#import "WebViewController.h"
#import "ShareTableViewCell.h"
#import "MailViewController.h"

@interface SettingsViewController ()

@property (strong, nonatomic) IBOutlet UISwitch *facebookSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *youtubeSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *tutorialSwitch;

- (IBAction)tutorialSwitchChanged:(UISwitch *)sender;
- (IBAction)facebookSwitchChanged:(UISwitch *)sender;
- (IBAction)youtubeSwitchChanged:(UISwitch *)sender;

@end

@implementation SettingsViewController

#pragma mark - View Lifecycle

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookLoginSuccess) name:FacebookDidLoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookLoginFailed) name:FacebookDidLoginFail object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tutorialSwitchChanged:(UISwitch *)sender {
    DDLogVerbose(@"Tutorial Switch");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (sender.on) {
        [defaults setBool:YES forKey:kTutorialDefaultskey];
    } else {
        [defaults setBool:NO forKey:kTutorialDefaultskey];
    }
    
    [defaults synchronize];
    
}

- (IBAction)facebookSwitchChanged:(UISwitch *)sender {
    DDLogVerbose(@"Facebook Switch");
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (sender.on) {
        [appDelegate openFacebookSession];
    } else {
        [appDelegate closeFacebookSession];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logged Out" message:@"You have been logged out from Facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)facebookLoginSuccess {
    [SVProgressHUD showSuccessWithStatus:@"Logged in!"];
}

- (void)facebookLoginFailed {
    [SVProgressHUD showErrorWithStatus:@"Failed to login Facebook."];
    self.facebookSwitch.on = NO;
}

- (IBAction)youtubeSwitchChanged:(UISwitch *)sender {
    DDLogVerbose(@"YouTube Switch");
    
    if (sender.on) {
        
        UIViewController *loginView = [[YouTubeController sharedInstance] loginViewControllerWithCompletionHandler:^(NSError *error) {
            if (!error) {
                [SVProgressHUD showSuccessWithStatus:@"Logged in!"];
                sender.on = YES;
            } else {
                if ([error code] != -1000) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                } else {
                    sender.on = NO;
                }
            }
        }];
        
        [self.navigationController pushViewController:loginView animated:YES];
        
    } else {
        
        [[YouTubeController sharedInstance] logout];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logged Out" message:@"You have been logged out from YouTube" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
}

#pragma mark - Table view datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 7;
    } else {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 6) {
            
            if (!cell) {
                cell = [[ShareTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ShareCell"];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.tutorialSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:kTutorialDefaultskey];
            
        }
        
    } else {
        
        if (!cell) {
            
            if (indexPath.row == 0) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HeaderCell"];
                return cell;
            } else {
                cell = [[ShareTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ShareCell"];
            }
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row == 1) {
            
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            self.facebookSwitch.on = [appDelegate isFacebookLoggedIn];
            
        } else if (indexPath.row == 2) {
        
            self.youtubeSwitch.on = [[YouTubeController sharedInstance] isLoggedInYouTube];
        }
        
    }
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 46)];
    backgroundView.backgroundColor = [UIColor colorWithRed:30.0f/255.0f green:30.0f/255.0f blue:30.0f/255.0f alpha:1];
    
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
    blackView.backgroundColor = [UIColor blackColor];
    
    [cell insertSubview:blackView atIndex:0];
    cell.backgroundView = backgroundView;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && indexPath.row == 0) { // HeaderCell
        return 30.0f;
    } else {
        return 46.0f;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 6) return;
        else if (indexPath.row == 1) [self showActionsheet];
        else if (indexPath.row == 2) [[iRate sharedInstance] promptForRating];
        else if (indexPath.row == 3) [self sendEmailTo:@"support@jamjarapps.com" withSubject:@"Momendio Support"];
        else if (indexPath.row == 4) [self sendEmailTo:@"contactus@jamjarapps.com" withSubject:@"Momendio Contact"];
        else [self performSegueWithIdentifier:@"SettingsToWeb" sender:indexPath];
    }
}

#pragma mark - Prepare for Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SettingsToWeb"]) {
        WebViewController *webViewController = segue.destinationViewController;
        NSIndexPath *indexPath = sender;
        
        if (indexPath.row == 0) {
            webViewController.title = @"Welcome Guide";
            webViewController.text = ABOUT_TEXT;
            webViewController.backgroundColor = [UIColor colorWithRed:30.0f/255.0f green:30.0f/255.0f blue:30.0f/255.0f alpha:1.0f];
            webViewController.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 3) {
            webViewController.title = @"Contact Us";
            webViewController.url = [[NSURL alloc] initWithString:@"http://www.momendio.com/contact"];
        } else {
            webViewController.title = @"Terms of Use";
        }
        
    }
}

#pragma mark - Helper Functions

- (void)setStyling {
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1];
}

- (void) showActionsheet {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Invite via SMS",
                            @"Invite via Email",
                            nil];
    popup.delegate = self;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)sendMessageTo {
    if([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.body = @"Check out Momendio, a great way to create videos worth sharing, on the go. Download it now from http://bit.ly/1g5cqk1";
        [messageController setMessageComposeDelegate:self];
        [self presentViewController:messageController animated:YES completion: ^ {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            messageController.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1];
        }];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendEmailTo:(NSString *)emailAddress withSubject:(NSString *)subjectString {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        // Generate Feedback Body
        NSString *deviceModel = [NSString stringWithFormat:@"Device Model: %@\n",[[UIDevice currentDevice] model]];
        NSString *deviceSystemName = [NSString stringWithFormat:@"Device System Name: %@\n",[[UIDevice currentDevice] systemName]];
        NSString *deviceSystemVersion = [NSString stringWithFormat:@"Device System Version: %@\n",[[UIDevice currentDevice] systemVersion]];
        NSString *appVersion = [NSString stringWithFormat:@"App Version: %@\n",[[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"]];
        
        
        NSString *feedbackBody = [NSString stringWithFormat:@"Feedback: \n\n\n\n\n%@%@%@%@",deviceModel,deviceSystemName,deviceSystemVersion,appVersion];
        
        MailViewController *mailer = [[MailViewController alloc] init];
        [mailer setToRecipients:@[emailAddress]];
        [mailer setSubject:subjectString];
        [mailer setMessageBody:feedbackBody isHTML:NO];
        mailer.bk_completionBlock = ^( MFMailComposeViewController *mailer, MFMailComposeResult result, NSError* error) {
            // TODO: Update Result
            if (result == MFMailComposeResultSent) {
                [SVProgressHUD showSuccessWithStatus:@"Email sent!"];
            }
        };
        
        [self presentViewController:mailer animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            mailer.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1];
        }];
        
    } else {
        [SVProgressHUD showErrorWithStatus:@"No email account set up in Settings app."];
    }
}

- (void)sendEmail {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        NSString *feedbackBody = [NSString stringWithFormat:@"Hey! \n\nI just downloaded Momendio on my iPhone.\n\nMomendio is a fast and fun way to create wonderful 24 sec videos worth sharing, on the go!\n\nSend them to everyone directly from the app to Facebook and Youtube!\n\nMomendio is available for iPhone and requires iOS 7.0. Compatible with iPhone 4s and above. The app is optimized for iPhone 5, 5c and 5s.\n\nGet it now from http://bit.ly/1g5cqk1"];
        
        MailViewController *mailer = [[MailViewController alloc] init];
        [mailer setSubject: @"Check out Momendio!"];
        [mailer setMessageBody:feedbackBody isHTML:NO];
        mailer.bk_completionBlock = ^( MFMailComposeViewController *mailer, MFMailComposeResult result, NSError* error) {
            // TODO: Update Result
            if (result == MFMailComposeResultSent) {
                [SVProgressHUD showSuccessWithStatus:@"Email sent!"];
            }
        };
        
        [self presentViewController:mailer animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            mailer.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:197.0f/255.0f blue:188.0f/255.0f alpha:1];
        }];
        
    } else {
        [SVProgressHUD showErrorWithStatus:@"No email account set up in Settings app."];
    }
}

#pragma UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self sendMessageTo];
            break;
            
        default:
            [self sendEmail];
            break;
    }
}

@end
