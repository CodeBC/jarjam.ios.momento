//
//  ShareViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 12/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ShareViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <FacebookSDK/FacebookSDK.h>
#import "APIClient.h"

#import "GalleryViewController.h"

#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "YouTubeController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"

#import "GTLUtilities.h"
#import "GTMHTTPUploadFetcher.h"
#import "GTMHTTPFetcherLogging.h"
#import "GTLYouTube.h"

#import "WebViewController.h"

@interface ShareViewController ()

@property (strong, nonatomic) MPMoviePlayerController *videoPlayer;
@property (strong, nonatomic) AVPlayerItem *playerItem;

@property (strong, atomic) ALAssetsLibrary* photoLibrary;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UIButton *saveCameraRollButton;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *youtubeButton;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)saveCameraRollButtonPressed:(id)sender;

- (IBAction)submitFeaturetteButtonPressed:(id)sender;
- (IBAction)youtubeButtonPressed:(id)sender;
- (IBAction)facebookButtonPressed:(id)sender;

- (IBAction)galleryButtonPressed:(id)sender;
- (IBAction)settingsButtonPressed:(id)sender;

@end

@implementation ShareViewController
{
    GTLServiceTicket *_uploadFileTicket;
    NSURL *_uploadLocationURL;  // URL for restarting an upload.
}

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setStyling];
    [self updateUILabels];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookLoginSuccess) name:FacebookDidLoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookLoginFailed) name:FacebookDidLoginFail object:nil];
    
    // Init Photo Library
    self.photoLibrary = [[ALAssetsLibrary alloc] init];
    

}

- (void)viewWillAppear:(BOOL)animated {
    [self setupVideoPlayBackIfPossible];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)backButtonPressed {
    NSArray *viewControllers =  self.navigationController.viewControllers;
    if ([viewControllers[viewControllers.count-2] isKindOfClass:[GalleryViewController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)saveCameraRollButtonPressed:(id)sender {
    
    UIAlertView *alertView = [UIAlertView bk_alertViewWithTitle:@"Save to Camera Roll" message:@"Are your sure?"];
    
    [alertView bk_addButtonWithTitle:@"Cancel" handler:^{
        
    }];
    
    [alertView bk_addButtonWithTitle:@"Save" handler:^{
    
        [SVProgressHUD show];
        [self.photoLibrary saveVideoPathURL:[NSURL URLWithString:[self.currentProject getRelativePublishedURL]] toAlbum:@"Momendio" withCompletionBlock:^(NSError *error) {
            if (!error) {
                [SVProgressHUD showSuccessWithStatus:@"Saved to Camera Roll!"];
            } else {
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
            }
        }];
        
    }];
    
    [alertView show];
}

- (IBAction)submitFeaturetteButtonPressed:(id)sender {
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebView"];
    webView.url = [NSURL URLWithString:@"http://submission1.momendio.com/submit_form.php"];
    webView.title = @"Submit Featurette";
    [self.navigationController pushViewController:webView animated:YES];
}

- (IBAction)galleryButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ShareToGallery" sender:self];
}

- (IBAction)settingsButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"ShareToSettings" sender:self];
}

- (IBAction)youtubeButtonPressed:(id)sender {
    if ([[YouTubeController sharedInstance].auth canAuthorize]) {
        
        self.youTubeService.authorizer = [YouTubeController sharedInstance].auth;
        [self uploadVideoFile];
        
    } else {
        
        YouTubeLoginViewController *viewController = [[YouTubeController sharedInstance] loginViewControllerWithCompletionHandler:^(NSError *error) {
            if (!error) {
                [SVProgressHUD showSuccessWithStatus:@"Logged In!"];
                [self youtubeButtonPressed:nil];
            } else {
                if ([error code] != -1000) {
                    [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                }
            }
        }];
        
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    }
}

- (void)uploadVideoFile {
    // Collect the metadata for the upload from the user interface.
    
    // Status.
    GTLYouTubeVideoStatus *status = [GTLYouTubeVideoStatus object];
    status.privacyStatus = @"public";
    
    // Snippet.
    GTLYouTubeVideoSnippet *snippet = [GTLYouTubeVideoSnippet object];
    snippet.title = self.currentProject.name;
    NSString *desc = self.currentProject.about;
    if ([desc length] > 0) {
        snippet.descriptionProperty = desc;
    }
    NSString *tagsStr = @"";
    if ([tagsStr length] > 0) {
        snippet.tags = [tagsStr componentsSeparatedByString:@","];
    }
    
    GTLYouTubeVideo *video = [GTLYouTubeVideo object];
    video.status = status;
    video.snippet = snippet;
    
    [self uploadVideoWithVideoObject:video
             resumeUploadLocationURL:nil];
}

- (void)uploadVideoWithVideoObject:(GTLYouTubeVideo *)video
           resumeUploadLocationURL:(NSURL *)locationURL {
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    // Get a file handle for the upload data.
    NSString *path = [self.currentProject getRelativePublishedURL];
    NSString *filename = [path lastPathComponent];
    NSError *error = nil;
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingFromURL:[NSURL URLWithString:path] error:&error];
    if (fileHandle) {
        NSString *mimeType = [self MIMETypeForFilename:filename
                                       defaultMIMEType:@"video/quicktime"];
        GTLUploadParameters *uploadParameters =
        [GTLUploadParameters uploadParametersWithFileHandle:fileHandle
                                                   MIMEType:mimeType];
        uploadParameters.uploadLocationURL = locationURL;
        
        GTLQueryYouTube *query = [GTLQueryYouTube queryForVideosInsertWithObject:video
                                                                            part:@"snippet,status"
                                                                uploadParameters:uploadParameters];
        
        GTLServiceYouTube *service = self.youTubeService;
        _uploadFileTicket = [service executeQuery:query
                                completionHandler:^(GTLServiceTicket *ticket,
                                                    GTLYouTubeVideo *uploadedVideo,
                                                    NSError *error) {
            // Callback
            _uploadFileTicket = nil;
            if (error == nil) {
                [SVProgressHUD dismiss];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Video uploaded successfully!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];

            } else {
                [SVProgressHUD showErrorWithStatus:@"Upload failed"];
            }
            
            _uploadLocationURL = nil;
        }];
        
        _uploadFileTicket.uploadProgressBlock = ^(GTLServiceTicket *ticket,
                                                  unsigned long long numberOfBytesRead,
                                                  unsigned long long dataLength) {
            NSLog(@"Sent %lld of %lld bytes", numberOfBytesRead, dataLength);
            [SVProgressHUD showProgress:((double)numberOfBytesRead/(double)dataLength) status:[NSString stringWithFormat:@"Uploading... (%.1f/%.1f)KB",(double)numberOfBytesRead/1024.0f,(double)dataLength/1024.0f] maskType:SVProgressHUDMaskTypeGradient];
        };
        
        // To allow restarting after stopping, we need to track the upload location
        // URL.
        //
        // For compatibility with systems that do not support Objective-C blocks
        // (iOS 3 and Mac OS X 10.5), the location URL may also be obtained in the
        // progress callback as ((GTMHTTPUploadFetcher *)[ticket objectFetcher]).locationURL
        
        GTMHTTPUploadFetcher *uploadFetcher = (GTMHTTPUploadFetcher *)[_uploadFileTicket objectFetcher];
        uploadFetcher.locationChangeBlock = ^(NSURL *url) {
            _uploadLocationURL = url;
        };
        
    } else {
        // Could not read file data.
        DDLogError(@"%@: %@, %@",THIS_FILE,THIS_METHOD,[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Error reading video file."];
    }
}

// Get a service object with the current username/password.
//
// A "service" object handles networking tasks.  Service objects
// contain user authentication information as well as networking
// state information such as cookies set by the server in response
// to queries.

- (GTLServiceYouTube *)youTubeService {
    static GTLServiceYouTube *service;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[GTLServiceYouTube alloc] init];
        
        // Have the service object set tickets to fetch consecutive pages
        // of the feed so we do not need to manually fetch them.
        service.shouldFetchNextPages = YES;
        
        // Have the service object set tickets to retry temporary error conditions
        // automatically.
        service.retryEnabled = YES;
    });
    return service;
}

- (NSString *)MIMETypeForFilename:(NSString *)filename
                  defaultMIMEType:(NSString *)defaultType {
    NSString *result = defaultType;
    NSString *extension = [filename pathExtension];
    CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,
                                                            (__bridge CFStringRef)extension, NULL);
    if (uti) {
        CFStringRef cfMIMEType = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType);
        if (cfMIMEType) {
            result = CFBridgingRelease(cfMIMEType);
        }
        CFRelease(uti);
    }
    return result;
}



- (IBAction)facebookButtonPressed:(id)sender {
    if (FBSession.activeSession.state != FBSessionStateOpen) {
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate openFacebookSession];
        
    } else {
    
        DDLogVerbose(@"%@: %@, %@",THIS_FILE,THIS_METHOD, [self.currentProject getRelativePublishedURL]);
        NSData *videoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[self.currentProject getRelativePublishedURL]]];
        
        if (!videoData) {
            [SVProgressHUD showErrorWithStatus:@"Video could not be found"];
            return;
        }
        
        [[FBSession activeSession] requestNewPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession *session, NSError *error) {
            NSDictionary *parameters = @{ @"video.mov": videoData,
                                          @"title": self.currentProject.name,
                                          @"description": @"" };
            
            FBRequest *request = [FBRequest requestWithGraphPath:@"me/videos" parameters:parameters HTTPMethod:@"POST"];
            [SVProgressHUD showWithStatus:@"Uploading Video to Facebook..." maskType:SVProgressHUDMaskTypeGradient];
            FBRequestConnection *requestConnection = [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            }];
            [requestConnection cancel];
            
            NSMutableURLRequest *urlRequest = requestConnection.urlRequest;
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSError *error;
                NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                if (!error && [responseObject objectForKey:@"id"]) {
                    DDLogVerbose(@"%@: %@, %@",THIS_FILE,THIS_METHOD,responseDict);
                    
                    [SVProgressHUD dismiss];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Video uploaded successfully!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                } else {
                    [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
            }];
            
            [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                
                NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
                [SVProgressHUD showProgress:((double)totalBytesWritten/(double)totalBytesExpectedToWrite) status:[NSString stringWithFormat:@"Uploading... (%.1f/%.1f)KB",(double)totalBytesWritten/1024.0f,(double)totalBytesExpectedToWrite/1024.0f] maskType:SVProgressHUDMaskTypeGradient];
            }];
            [[APIClient sharedInstance] enqueueHTTPRequestOperation:operation];
            
        }];
    }
}

#pragma mark - Notification Handlers

- (void)facebookLoginSuccess {
    [self facebookButtonPressed:nil];
}

- (void)facebookLoginFailed {
    [SVProgressHUD showErrorWithStatus:@"Failed to login Facebook."];
}

#pragma mark - Helper Functions

- (void)setStyling {
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.scrollView setContentSize:self.contentView.frame.size];
    
    [self.titleLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:17.0f]];
    [self.saveCameraRollButton.titleLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:16.0f]];
    [self.facebookButton.titleLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:16.0f]];
    [self.youtubeButton.titleLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:16.0f]];
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:16.0f]];
    
}

- (void)updateUILabels {
    self.titleLabel.text = self.currentProject.name;
    [self.titleLabel sizeToFit];
}

- (void)setupVideoPlayBackIfPossible {
    if (!self.videoPlayer && self.currentProject && [self.currentProject getRelativePublishedURL]) {
        self.videoPlayer = [[MPMoviePlayerController alloc] initWithContentURL: [NSURL URLWithString:[self.currentProject getRelativePublishedURL]]];
        self.videoPlayer.allowsAirPlay = NO;
        self.videoPlayer.shouldAutoplay = NO;
        [self.videoPlayer prepareToPlay];
        [self.videoPlayer.view setFrame: self.videoView.bounds];  // player's frame must match parent's
        [self.videoView addSubview: self.videoPlayer.view];
    }
}

@end
