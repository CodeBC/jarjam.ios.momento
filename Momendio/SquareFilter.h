//
//  SquareFilter.h
//  Momendio
//
//  Created by Nur Iman Izam on 14/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GPUImageCropFilter.h"

@interface SquareFilter : GPUImageCropFilter

- (void)setCropFor720p;
- (void)setCropFor480p;

@end
