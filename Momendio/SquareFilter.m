//
//  SquareFilter.m
//  Momendio
//
//  Created by Nur Iman Izam on 14/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "SquareFilter.h"

@implementation SquareFilter

- (id)init
{
    return [super initWithCropRegion:CGRectMake(0.0f, 0.28125f, 1.0f, 0.5625f)];
}

- (void)setCropFor720p {
    self.cropRegion = CGRectMake(0.0f, 0.28125f, 1.0f, 0.5625f);
}

- (void)setCropFor480p {
    self.cropRegion = CGRectMake(0.0f, 0.125f, 1.0f, 0.75f);
}

@end
