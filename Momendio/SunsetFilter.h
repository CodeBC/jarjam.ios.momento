//
//  SunsetFilter.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 30/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GPUImageFilterGroup.h"

@interface SunsetFilter : GPUImageFilterGroup
{
    GPUImagePicture *lookupImageSource;
}

@end
