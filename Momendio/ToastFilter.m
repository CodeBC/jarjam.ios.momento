//
//  ToastFilter.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 30/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "ToastFilter.h"
#import "GPUImagePicture.h"
#import "GPUImageLookupFilter.h"

@implementation ToastFilter

- (id)init
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
    UIImage *image = [UIImage imageNamed:@"lookup_toast.png"];
#else
    NSImage *image = [NSImage imageNamed:@"lookup_toast.png"];
#endif
    
    NSAssert(image, @"To use ToastFilter you need to add lookup_toast.png in your application bundle.");
    
    lookupImageSource = [[GPUImagePicture alloc] initWithImage:image];
    GPUImageLookupFilter *lookupFilter = [[GPUImageLookupFilter alloc] init];
    
    [lookupImageSource addTarget:lookupFilter atTextureLocation:1];
    [lookupImageSource processImage];
    
    self.initialFilters = [NSArray arrayWithObjects:lookupFilter, nil];
    self.terminalFilter = lookupFilter;
    
    return self;
}

@end
