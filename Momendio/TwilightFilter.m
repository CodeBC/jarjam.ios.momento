//
//  TwilightFilter.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 11/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "TwilightFilter.h"
#import "GPUImagePicture.h"
#import "GPUImageLookupFilter.h"

@implementation TwilightFilter

- (id)init
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
    UIImage *image = [UIImage imageNamed:@"lookup_twilight.png"];
#else
    NSImage *image = [NSImage imageNamed:@"lookup_twilight.png"];
#endif
    
    grayscaleFilter = [GPUImageGrayscaleFilter new];
    [self addFilter:grayscaleFilter];
    
    multiplyBlendFilter = [GPUImageMultiplyBlendFilter new];
    [self addFilter:multiplyBlendFilter];
    
    [grayscaleFilter addTarget:multiplyBlendFilter];
    
    NSAssert(image, @"To use TwilightFilter you need to add lookup_twilight.png in your application bundle.");
    
    lookupImageSource = [[GPUImagePicture alloc] initWithImage:image];
    GPUImageLookupFilter *lookupFilter = [[GPUImageLookupFilter alloc] init];
    [self addFilter:lookupFilter];
    
    [lookupImageSource addTarget:lookupFilter atTextureLocation:1];
    [lookupImageSource processImage];
    
    [multiplyBlendFilter addTarget:lookupFilter];
    
    self.initialFilters = @[grayscaleFilter,multiplyBlendFilter,lookupFilter];
    self.terminalFilter = lookupFilter;
    
    return self;
}

@end
