//
//  UIImage+AVThumbnail.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 2/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface UIImage (AVThumbnail)

+ (UIImage *)thumbnailFromVideoAtURL:(NSURL *)contentURL;

@end
