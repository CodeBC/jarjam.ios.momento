//
//  Video.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 2/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Project;

@interface Video : NSManagedObject

@property (nonatomic, retain) NSString * localURLString;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) NSNumber * filterType;
@property (nonatomic, retain) NSNumber * cameraPosition;
@property (nonatomic, retain) NSData * thumbnailData;
@property (nonatomic, retain) Project *project;

- (NSString *) getRelativeLocalURL;
- (void)refreshThumbnail;

@end
