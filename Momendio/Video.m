//
//  Video.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 2/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "Video.h"
#import "Project.h"


@implementation Video

@dynamic localURLString;
@dynamic sortOrder;
@dynamic filterType;
@dynamic cameraPosition;
@dynamic thumbnailData;
@dynamic project;

- (void)refreshThumbnail {
    UIImage *thumbnailImage = [UIImage thumbnailFromVideoAtURL:[NSURL URLWithString:self.localURLString]];
    if (thumbnailImage) {
        self.thumbnailData = UIImageJPEGRepresentation(thumbnailImage,0.5);
    }
}

- (NSString *) getRelativeLocalURL
{
    // Split current published local url
    NSString *filePath = [[self.localURLString componentsSeparatedByString:@"/Documents/"] lastObject];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    NSString *relativePath = [NSString stringWithFormat:@"file://%@/%@", documentsDirectory, filePath];
    return relativePath;
}


@end
