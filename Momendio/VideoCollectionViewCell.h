//
//  VideoCollectionViewCell.h
//  Momendio
//
//  Created by Nur Iman Izam Othman on 2/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "PSTCollectionViewCell.h"

@interface VideoCollectionViewCell : PSTCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *playImageView;
@property (weak, nonatomic) IBOutlet UIImageView *playBigImageView;
@property (strong, nonatomic) IBOutlet UILabel *clipNumberLabel;

- (void)setStyling;

@end
