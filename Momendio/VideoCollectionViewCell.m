//
//  VideoCollectionViewCell.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 2/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "VideoCollectionViewCell.h"

@implementation VideoCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setStyling {
    self.thumbnailImageView.clipsToBounds = YES;
    self.thumbnailImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.thumbnailImageView.layer.borderWidth = 0.5f;
    
    for (UIGestureRecognizer *recognizer in self.gestureRecognizers) {
        [self removeGestureRecognizer:recognizer];
    }
}

@end
