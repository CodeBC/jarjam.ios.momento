//
//  VideoTableViewCell.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 29/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "VideoTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation VideoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setStyling {
    
    self.previewImageView.layer.cornerRadius = 1.0f;
    self.previewImageView.layer.masksToBounds = YES;
    self.previewImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.previewImageView.layer.borderWidth = 0.5f;
    
    [self.nameLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:15.0f]];
    
    self.layer.borderColor = [[UIColor colorWithRed:84.0f/255.0f green:179.0f/255.0f blue:169.0f/255.0f alpha:1.0f] CGColor];
}

@end
