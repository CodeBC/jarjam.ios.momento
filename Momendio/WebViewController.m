//
//  WebViewController.m
//  Momendio
//
//  Created by Nur Iman Izam Othman on 13/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:self.url];
    [self.webView loadRequest:urlRequest];
    [self setStyling];
}

- (void)viewWillAppear:(BOOL)animated {
    if (self.url) {

        self.textView.hidden = YES;
    } else if (self.text) {
        self.webView.hidden = YES;
        self.textView.text = self.text;
        self.view.backgroundColor = self.backgroundColor;
        self.textView.textColor = self.textColor;
    }

    self.textView.font = [UIFont fontWithName:@"Avenir-Light" size:15.0f];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Helper Functions

- (void)setStyling {
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    [backButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem = backButton;
}

@end
