//
//  YouTubeController.h
//  Momendio
//
//  Created by Nur Iman Izam on 28/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YouTubeLoginViewController.h"
//#import "AFNetworking.h"

@interface YouTubeController : NSObject

@property (nonatomic, strong) GTMOAuth2Authentication* auth;

+ (YouTubeController*)sharedInstance;

- (BOOL)isLoggedInYouTube;
- (YouTubeLoginViewController*)loginViewControllerWithCompletionHandler: (void(^)(NSError* error))completion;
- (void)authorizeAccessTokenWithcompleteHandler: (void(^)(void))completion;
- (void)logout;

@end
