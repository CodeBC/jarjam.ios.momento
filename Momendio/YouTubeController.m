//
//  YouTubeController.m
//  Momendio
//
//  Created by Nur Iman Izam on 28/7/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "YouTubeController.h"

@implementation YouTubeController

- (id)init
{
    self = [super init];
    if (self) {
        self.auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kYouTubeKeychainItemName
                                                                          clientID:kYouTubeClientID
                                                                      clientSecret:kYouTubeClientSecret];
        DDLogVerbose(@"%@",[self.auth description]);
    }
    return self;
}

- (BOOL)isLoggedInYouTube {
    if ([self.auth canAuthorize]) {
        return YES;
    } else {
        return  NO;
    }
}

+ (YouTubeController*)sharedInstance {
    static YouTubeController *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[YouTubeController alloc] init];
    });
    
    return __sharedInstance;
}

- (YouTubeLoginViewController*)loginViewControllerWithCompletionHandler: (void(^)(NSError* error))completion {
    
    YouTubeLoginViewController *viewController = [[YouTubeLoginViewController alloc] initWithScope:kYouTubeScope clientID:kYouTubeClientID clientSecret:kYouTubeClientSecret keychainItemName:kYouTubeKeychainItemName completionHandler:^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error) {
        
        if (error != nil) {
            NSLog(@"Login Error: %@", [error localizedDescription]);
            // Authentication failed
            if ([error code] == -1000) {
                NSLog(@"Login Window Closed");
            }
            
            completion(error);
            
        } else {
            // Authentication succeeded
            NSLog(@"Login Success %@", [auth accessToken]);
            self.auth = auth;
            
            completion(nil);
            
//            [self fetchUserEmailAddressWithCompleteHandler:^{
//                [[XMPPManager sharedInstance] setAccessToken:self.auth.accessToken];
//                [[XMPPManager sharedInstance] connect];
//            }];
        }
    }];
    
    return viewController;
}

- (void)authorizeAccessTokenWithcompleteHandler: (void(^)(void))completion {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@""]];
    [self.auth authorizeRequest:request completionHandler:^(NSError *error) {
        
        if (error)
            DDLogError(@"Authorize Error: %@",[error localizedDescription]);
        
        DDLogVerbose(@"Access Token: %@",[self.auth accessToken]);
        completion();
        
    }];
    
}

- (void)logout {
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kYouTubeKeychainItemName];
    [GTMOAuth2ViewControllerTouch revokeTokenForGoogleAuthentication:self.auth];
}

@end
